type TConfig =   record
    noWarnMode: boolean;
    showGrid: boolean;
    mapWidth: byte;
    mapHeight: byte;
end;

var config: TConfig;

procedure StoreConfig;
begin
    NeoSave(CONFIG_FILE,word(@config),SizeOf(TConfig));
    reload := true;
end;

procedure DefaultConfig;
begin
    config.noWarnMode := false;
    config.showGrid := false;
    config.mapWidth := 20;
    config.mapHeight := 13;
    StoreConfig;
end;

procedure InitConfig;
var a:byte;
    size:cardinal;
    loaded:boolean;
begin
    tmpfile := CONFIG_FILE;
    loaded:=false;
    if NeoStatFile(@tmpfile,size,a) then 
        if size=sizeOf(TConfig) then 
            loaded := NeoLoad(CONFIG_FILE,word(@config));
    if not loaded then DefaultConfig;
end;

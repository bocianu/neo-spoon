if mouse.LDown or mouse.RDown then begin  
    case mouse.areaId of
        0: CanvasClick;
    end;
end;

if (mouse.y>TMAP_TOP) and (mouse.y<TMAP_TOP+208) and (mouse.idle > SCROLL_MOUSE_DELAY) then begin
    if mouse.y<TMAP_TOP+SCROLL_MARGIN then if yOffTarget>0 then Dec(yOffTarget,SCROLLSTEP);
    if mouse.y>TMAP_TOP+(208-SCROLL_MARGIN) then if yOffTarget<(tileMapHeader.height-13)*16 then Inc(yOffTarget,SCROLLSTEP);
    if mouse.x<SCROLL_MARGIN then if xOffTarget>0 then Dec(xOffTarget,SCROLLSTEP);
    if mouse.x>(319-SCROLL_MARGIN) then if xOffTarget<(tileMapHeader.width-20)*16 then Inc(xOffTarget,SCROLLSTEP);
    reload:=true;
end;


if mouse.LUp then begin // left button up
    FixCursorView;
    //DrawCursor(GUI_CURSOR);
    //saveMapTmp;
end;

if mouse.MDown then begin  // middle button pressed down (on state change only)
    if mouse.areaId = 0 then CanvasMiddleClick;
end;

if (mouse.butchange = 0) and (mouse.but and 3 <> 0) then begin // button L/R pressed down (continuos event)
    if mouse.areaId = 0 then CanvasPressed;
end;

if mouse.wheel=1 then TilePrev;
if mouse.wheel=255 then TileNext;

setTmpKey(KEY_SPACE);
if IsKeyDown(tmpKey) then begin  // key pressed down 
    if GetTile(cursorX,cursorY)<>currentTile then 
        SetTile(cursorX,cursorY,currentTile);
end;

setTmpKey(KEY_DELETE);  
if IsKeyDown(tmpKey) then begin   // key pressed down 
    if GetTile(cursorX,cursorY)<>currentBackground then 
        SetTile(cursorX,cursorY,currentBackground);
end;

case key.code of
    //KEY_ESC: if helpScreen=$ff then SwitchTab(currentItemType+1);

    KEY_RIGHT: if key.shift then ShiftH(-1) else MoveCursor(1,0);
    KEY_LEFT: if key.shift then ShiftH(1) else MoveCursor(-1,0);
    KEY_DOWN: if key.shift then ShiftV(-1) else MoveCursor(0,1);
    KEY_UP: if key.shift then ShiftV(1) else MoveCursor(0,-1);

    KEY_G : begin
        config.showGrid := not config.showGrid;
        StoreConfig;
    end;
    
    KEY_P: if key.ctrl then begin   
        GetTile(cursorX,cursorY);
        reloadboard := true;
    end;

    KEY_PAGEUP, KEY_DOT: TilePrev;
    
    KEY_PAGEDOWN, KEY_COMMA: TileNext;

    KEY_BACKSPACE: if Confirm('Clear') then NewMap;

    KEY_HOME: begin 
        if cursorX>0 then cursorX := 0 else 
            if cursorY>0 then cursorY := 0;
        FixCursorView;
    end;

    KEY_END: begin 
        if cursorX<tileMapHeader.width-1 then cursorX := tileMapHeader.width - 1 else 
            if cursorY<tileMapHeader.height-1 then cursorY := tileMapHeader.height - 1;
        FixCursorView;
    end;

    KEY_X: SwapColBack;


end;                    




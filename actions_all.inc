    
if mouse.LDown then 
    case mouse.areaId of
        // tabs
        1:key.code := KEY_F1;
        2:key.code := KEY_F2;
        3:key.code := KEY_F3;
        4:key.code := KEY_F4;
        5:key.code := KEY_F5;
        6:key.code := KEY_F6;
        // help
        8: begin key.shift:=true; key.code := KEY_SLASH; end; 
    end;

case key.code of

    KEY_TAB: if key.shift then SwitchTab(currentTab - 1)
        else SwitchTab(currentTab + 1);
    KEY_F1: SwitchTab(0);
    KEY_F2: SwitchTab(1);
    KEY_F3: SwitchTab(2);
    KEY_F4: SwitchTab(3);
    KEY_F5: SwitchTab(4);
    KEY_F6: SwitchTab(5);

    KEY_S: if key.ctrl then begin // ctrl S
        if gfxfile='' then GetFileName(gfxfile,'Saving file as:');
        if gfxfile<>'' then begin
            if (GFXSave(gfxfile)) then begin
                okMessage := 'File saved successfully.';
            end else Error('Error saving file.');                    
        end else Error('Operation Canceled.');                    
        reload := true;
    end;

    KEY_O: if key.ctrl then begin // ctrl O
        GetFileName(tmpfile,'Loading File:');
        if tmpfile<>'' then begin 
            if (NeoLoad(tmpfile, NEO_GFX_RAM)) then begin
                ReadHeader;
                if IsPaletteStored then RestorePalettes else StorePalettes;
                gfxfile := tmpfile;
                currentItemType := $ff;
            end else begin
                Error('Error opening file.');
            end;
        end else Error('Operation Canceled.');        
        reload := true;
    end;

    KEY_D: if key.ctrl then begin // ctrl D
        GetFileName(tmpfile,'Saving file as:');
        if tmpfile<>'' then begin 
            if (GFXSave(tmpfile)) then begin
                gfxfile := tmpfile;
                okMessage := 'File saved successfully.';
            end else Error('Error saving file.');    
        end else Error('Operation Canceled.');        
        reload := true;
    end;

    KEY_N: if key.ctrl then begin // ctrl N
        if Confirm('Clear') then NewSheet;
    end;

    KEY_Q: if key.ctrl then begin // ctrl Q
        if Confirm('Exit') then quit:=true;
    end;

    KEY_SLASH: if key.shift then ToggleHelp;

    KEY_ESC: if helpScreen<>$ff then ToggleHelp;

    KEY_F10: begin UpdateItem; LoadNeo('tspoon.neo'); end;
end;
type TGfxHeader = record
    format:byte;
    counts:array [0..2] of byte;
end;

type TMouseArea = record
    tab: byte;
    x: word;
    y: word;
    w: word;
    h: byte;
    areaId: byte;
end; 

type TMouse = record
    x:word;
    y:word;
    but:byte;
    butchange:byte;
    wheel:byte;
    idle:cardinal;
    areaId:byte;
    dblclick:byte;
    LDown:boolean;
    LUp:boolean;
    RDown:boolean;
    RUp:boolean;
    MDown:boolean;
    MUp:boolean;
end;
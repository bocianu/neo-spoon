if mouse.LDown or mouse.RDown then 
    case mouse.areaId of
        5: TilesClick;
        6: begin key.code := KEY_L; key.ctrl := true; end;
        7: begin key.code := KEY_P; key.ctrl := true; end;
        9: key.code := KEY_F10;
        10: SolidsClick;
    end;

if mouse.wheel=1 then TilePrev;
if mouse.wheel=255 then TileNext;

    case key.code of
        KEY_L: if key.ctrl then begin // ctrl L
            GetFileName(tmpfile,'Loading GFX file:');
            if tmpfile<>'' then begin 
                if (NeoLoad(tmpfile, NEO_GFX_RAM)) then begin
                    ReadHeader;
                    if IsPaletteStored then RestorePalettes else StorePalettes;
                    ReadHeader;
                    gfxfile := tmpfile;
                end else begin
                    Error('Error opening file.');
                end;
            end else Error('Operation Canceled.');        
            reload := true;
        end;        
        KEY_P: if key.ctrl then LoadPalette; // ctrl P

    KEY_RIGHT: TileNext;

    KEY_LEFT: TilePrev;

    KEY_DOWN: begin
        if gfxHeader.counts[0]>0 then begin
            currentTile := currentTile + 16;
            if currentTile >= gfxHeader.counts[0] then currentTile := $f0 + currentTile mod 16;
            reloadboard := true;
        end;
    end;

    KEY_UP: begin
        if gfxHeader.counts[0]>0 then begin
            currentTile := currentTile - 16;
            if currentTile < $fe then begin
                //currentTile := currentTile + (16 * ((gfxHeader.counts[0] div 16)+2));
                while (currentTile >= gfxHeader.counts[0]) and (currentTile < $f0) do currentTile := currentTile - 16;
                //if currentTile >= gfxHeader.counts[0] then currentTile := $f0 + currentTile mod 16;
            end;
            reloadboard := true;
        end;
    end;

    KEY_B: begin
        currentBackground := currentTile;
        reloadboard := true;
        //SelectCurrent;
    end;



    end;

if mouse.LDown then 
    case mouse.areaId of
        7: TilesClick;
        26: key.code := KEY_I;
        27: key.code := KEY_L;
        28: key.code := KEY_S;
        29: key.code := KEY_P;
    end;
    
if mouse.wheel=1 then ItemPrev;
if mouse.wheel=255 then ItemNext;

currentTabType := currentTab - 1;
cursorMode := cursorMode or CUR_KEY;

case key.code of
    KEY_EQUAL: AddItem(currentTabType,gfxHeader.counts[currentTabType]);

    KEY_INSERT : AddItem(currentTabType,currentTabItem[currentTabType]);

    KEY_DELETE : if gfxHeader.counts[currentTabType]>0 then
            if Confirm('Delete') then 
                DelItem(currentTabType,currentTabItem[currentTabType]);

    KEY_C: if key.ctrl then GetCopy(currentTabType,currentTabItem[currentTabType]);

    KEY_V: if key.ctrl then 
            if Confirm('Overwrite') then 
                PasteCopy(currentTabType,currentTabItem[currentTabType]);

    KEY_I: ImportElements;

    KEY_L: LoadPalette;

    KEY_P: NextPage;

    KEY_S: if not key.ctrl then SavePalette;

    KEY_RIGHT: ItemNext;

    KEY_LEFT: ItemPrev;

    KEY_DOWN: begin
        currentTabItem[currentTabType] := currentTabItem[currentTabType] + typeCols[currentTabType];
        if currentTabItem[currentTabType] >= gfxHeader.counts[currentTabType] then currentTabItem[currentTabType] := currentTabItem[currentTabType] mod typeCols[currentTabType];
        reloadboard := true;
    end;

    KEY_UP: begin
        currentTabItem[currentTabType] := currentTabItem[currentTabType] - typeCols[currentTabType];
        if currentTabItem[currentTabType] < 0 then begin
            currentTabItem[currentTabType] := currentTabItem[currentTabType] + (typeCols[currentTabType] * ((gfxHeader.counts[currentTabType] div typeCols[currentTabType])+1));
            while currentTabItem[currentTabType] >= gfxHeader.counts[currentTabType] do currentTabItem[currentTabType] := currentTabItem[currentTabType] - typeCols[currentTabType];
        end;
        reloadboard := true;
    end;

    KEY_ENTER: begin
        if gfxHeader.counts[currentTabType]>0 then begin
            EditCurrent;
        end;
    end;
end;

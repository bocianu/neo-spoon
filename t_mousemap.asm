    icl 't_const.inc'

;;          tab, x, y, w, h, eventID
    
    dta     TAB_MAP,a(0),a(TMAP_TOP),a(320),208,0 ; tiles / sprites list

    // TABS
    dta     TAB_ALL,a(1),              a(0),   a(TAB_WIDTH),    11,     1  ; tab 0
    dta     TAB_ALL,a(TAB_WIDTH*1),    a(0),   a(TAB_WIDTH),    11,     2  ; tab 1
    dta     TAB_ALL,a(TAB_WIDTH*2),    a(0),   a(TAB_WIDTH),    11,     3  ; tab 2
    dta     TAB_ALL,a(TAB_WIDTH*3),    a(0),   a(TAB_WIDTH),    11,     4  ; tab 3

    // TILES 
    dta     TAB_TILES,a(1),a(15),a(318),165,5 ; tiles / sprites list
    dta     TAB_TILES,a(3),a(202),a(77),20,6  ; Load Tiles
    dta     TAB_TILES,a(84),a(202),a(77),20,7  ; Load Palette
    dta     TAB_TILES,a(214),a(202),a(100),20,9  ; load Spoon
    dta     TAB_TILES,a(1),a(180),a(318),20,10  ; solids

    dta     TAB_ALL,a(312),a(0),a(8),11,8 ; help

    // config
    dta     TAB_CONFIG,a(16),a(30),a(200),8,20   ; Disable
    dta     TAB_CONFIG,a(16),a(46),a(200),8,21   ; Grid
    dta     TAB_CONFIG,a(16),a(62),a(200),8,22   ; Width
    dta     TAB_CONFIG,a(16),a(78),a(200),8,23   ; height


    dta     0 ;footer

if mouse.LDown then 
    case mouse.areaId of
        20:key.code := KEY_D;
        21:key.code := KEY_G;
    end;

if (mouse.wheel=1) and (mouse.areaId=22) then key.code := KEY_W;
if (mouse.wheel=255) and (mouse.areaId=22) then begin key.code := KEY_W; key.shift:=true; end;
if (mouse.wheel=1) and (mouse.areaId=23) then key.code := KEY_H;
if (mouse.wheel=255) and (mouse.areaId=23) then begin key.code := KEY_H; key.shift:=true; end;

case key.code of
    
    KEY_R: if key.ctrl then begin // ctrl R
        DefaultConfig;
        reload := true;
    end;
    
    KEY_D: begin
        config.noWarnMode := not config.noWarnMode;
        StoreConfig;
    end;

    KEY_G: begin
        config.showGrid := not config.showGrid;
        StoreConfig;
    end;

    KEY_H: begin
        if key.shift then begin
            if config.mapHeight > 0 then config.mapHeight := config.mapHeight - 1;
        end else begin
            if config.mapHeight < 255 then 
                if ((config.mapHeight + 1) * config.mapWidth) <= TILEMAP_MAXSIZE then 
                    config.mapHeight := config.mapHeight + 1;
        end;
        StoreConfig;
        reload := true;
    end;

    KEY_W: begin
        if key.shift then begin
            if config.mapWidth > 0 then config.mapWidth := config.mapWidth - 1;
        end else begin
            if config.mapWidth < 255 then 
                if ((config.mapWidth + 1) * config.mapWidth) <= TILEMAP_MAXSIZE then 
                    config.mapWidth := config.mapWidth + 1;
        end;
        StoreConfig;
        reload := true;
    end;


end;

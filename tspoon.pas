uses crt,neo6502,neo6502math,neo6502keys;
const
{$i t_const.inc}
    VER = '0.62'; 
    CONFIG_FILE = '/.tspoon.cnf';
    TMPMAP_FILE = '/.map.tmp';
    SPOON_NEO = 'spoon.neo';

{$i types.inc}

const   
    tabs:array [0..TAB_COUNT-1] of byte = (TAB_FILE,TAB_MAP,TAB_TILES,TAB_CONFIG);
    tabnames:array [0..TAB_COUNT-1] of string[6] = ('File','Map','Tiles','Config');
    hexChars:array [0..15] of char = ('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
    paletteTiles: array[0..15] of byte = (0,1,2,3,4,5,6,7,8,9,$a,$b,$c,$d,$e,$f);

var 
    reload,reloadboard,quit:boolean;
    inputDelay:byte;
    joyCount:byte;
    joyDelay:byte;
    currentTile:byte;
    currentTab:byte;
    currentTool:byte;
    currentBackground:byte;
    gfxHeader: TGfxHeader absolute GFX_HEADER_ADDRESS;
    importHeader: TGfxHeader;
    tileMapHeader: TTileMapHeader absolute TILEMAP_HEADER;
    tileMap: array[0..0] of byte absolute TILEMAP_ADDRESS;
    tileMapSize, tileMapWidth, tileMapHeight, tileMapXoff, tileMapYoff: word;
    xOffTarget,yOffTarget: word;
    okMessage,tmpstr:string[56];
    gfxfile,mapfile,tmpfile:TString;
    tmpKey,key: TKey;
    frame:byte;
    cursorX,cursorY:byte;
    clickedTileX,clickedTileY:byte;
    delayedReload:word;
    //clipboardData:array [0..511] of byte;
    //undoBuffer,undoOrigin:array [0..511] of byte;
    //undoAddresses:array [0..UNDO_MAX-1] of word;
    undoFirstId:byte;
    undoLastId:byte;
    undoCount:byte;
    redoCount:byte;

    mArea:^TMouseArea;
    mouseDelay:byte;
    prevMouseY,prevMouseX:word;
    mButPrev:byte;
    mWheelPrev:byte;
    mouse:TMouse;
    mouseIdle:cardinal;
    drawStart:boolean;

    cursorMode:byte;
    helpScreen:byte;    
    spoonExists:boolean;

{$i t_config.inc}
{$i t_helpers.inc}
{$i t_tiles.inc}
//{$i undo.inc}
{$i t_gui.inc}
{$i t_help.inc}
{$i mouse.inc}
{$r t_resources.rc}

begin

    reload:=true;
    quit:=false;
    currentTab:=0;
    inputDelay:=INPUT_DELAY;
    cursorMode:=CUR_KEY;
    helpScreen:=$ff;

    NeoSetChar(192,@charOff);
    NeoSetChar(193,@charOn);

    InitConfig;
    NeoSetDefaults(0,0,1,1,0);
    NeoSelectCursor(0);
    ValidateBrushes;

    spoonExists := FileExists(SPOON_NEO);

    NeoResetPalette;
    if IsGfxDataLoaded then begin
        ReadHeader;
        if IsPaletteStored then RestorePalettes;
    end else NewSheet;
    RefreshGuiPalette;
            
    if not LoadMapTmp then NewMap;
    
    repeat                                          // MAIN LOOP
        NeoShowCursor(cursorMode shr 1);
        NeoWaitForVblank;
        BlinkCursor;

        if delayedReload = 1 then reload:=true;     
        if delayedReload > 0 then Dec(delayedReload);

        if (yOffTarget <> tileMapYoff) or (xOffTarget <> tileMapXoff) then MoveMap;


        if reload then begin

            while keypressed do ReadKey; // clear key buffer

            ValidateBrushes;
            NeoSetColor(GUI_BACK);
            NeoSetSolidFlag(1);
            NeoDrawRect(0,0,319,239); // clear
            
            ShowMainScreen;

            if helpScreen=$ff then begin
                case currentTab of
                    0:ShowFileTab;
                    1:ShowMapTab;                
                    2:ShowTilesTab;     
                    3:ShowConfigTab;           
                end;
            end else ShowHelp(helpScreen);

            if okMessage<>'' then begin
                OK(okMessage);
                okMessage:='';
            end;

            reload := false;
            reloadboard := false;
        end;

        if reloadboard then begin
            case currentTab of
                1:ShowMapTab;                 
                2:begin
                    DrawElements;
                    DrawSolids;
                    DrawTileInfo;
                end; 
            end;
            reloadboard := false;
        end;

        if inputDelay > 0 then begin
            Dec(inputDelay);
            mouse.butchange:=0;
            key.code:=0;
            mouse.but:=0;
            mouse.wheel:=0;
        end else GetUserInput;  // reads all inputs returning the user action in key and mouse record

        //gotoxy(1,1);Write(mouse.areaId);

        // interact on tabs

        if currentTab = 1 then begin                                                  // MAP TAB
            {$i t_actions_map.inc}
        end;

        if currentTab = 2 then begin                                                  // MAP TILES
            {$i t_actions_tiles.inc}
        end;

        if currentTab = 3 then begin                                                  // CONFIG TAB
            {$i t_actions_config.inc}
        end;

        {$i t_actions_all.inc}

    until quit;

    while keypressed do readkey;
    NeoShowCursor(0);
    NeoResetPalette;
    ClrScr;
end.


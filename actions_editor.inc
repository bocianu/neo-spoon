if mouse.LDown then begin  
        case mouse.areaId of
            // canvas
            0: CanvasClick;
            9: PaletteClick;
            10 : key.code := KEY_P;
            11 : key.code := KEY_R;
            12 : key.code := KEY_I;
            13 : key.code := KEY_S;
            14 : begin key.ctrl:=true; key.code := KEY_H; end; 
            15 : begin key.ctrl:=true; key.code := KEY_V; end; 
            16 : begin key.ctrl:=true; key.code := KEY_R; end; 
            21 : key.code := KEY_GRAVE;
        end;
end;

if mouse.LUp then begin // left button up
    ClearCursor;
    FixCursor;
    DrawCursor(GUI_CURSOR);
end;

if mouse.RDown then begin  // right button pressed down (on state change only)
    case mouse.areaId of
        9: PaletteClick;
        21 : begin key.shift:=true; key.code := KEY_GRAVE; end;
    end;
end;

if mouse.MDown then begin  // middle button pressed down (on state change only)
    if mouse.areaId = 0 then CanvasMiddleClick;
end;

if (mouse.butchange = 0) and (mouse.but and 3 <> 0) then begin // button L/R pressed down (continuos event)
    if mouse.areaId = 0 then CanvasPressed;
end;

if mouse.wheel <> 0 then ChooseColor((currentColor + mouse.wheel) and $f);

setTmpKey(KEY_SPACE);
if IsKeyDown(tmpKey) then begin  // key pressed down 
    cursorMode := CUR_KEY;
    if currentTool=TOOL_DRAW then begin
        //if GetCursorPixelsAll<>currentColor then 
        DrawPixel(cursorX,cursorY,currentColor);
    end;
end;

setTmpKey(KEY_DELETE);  
if IsKeyDown(tmpKey) then begin   // key pressed down 
    cursorMode := CUR_KEY;
    //if GetCursorPixelsAll(cursorX,cursorY)<>currentBackground then 
    DrawPixel(cursorX,cursorY,currentBackground);
end;

if currentTool = TOOL_SHIFT then begin
    case key.code of
        KEY_RIGHT: begin SaveUndoCmd(UNDO_SHIFTR); ShiftH(-1); end;
        KEY_LEFT: begin SaveUndoCmd(UNDO_SHIFTL); ShiftH(1); end;
        KEY_DOWN: begin SaveUndoCmd(UNDO_SHIFTD); ShiftV(-1); end;
        KEY_UP: begin SaveUndoCmd(UNDO_SHIFTU); ShiftV(1); end;
    end;
end;

case key.code of
    KEY_SPACE: if currentTool=TOOL_FILL then Fill(cursorX,cursorY);

    KEY_ESC: if helpScreen=$ff then SwitchTab(currentItemType+1);

    KEY_RIGHT: MoveCursor(1,0);
    KEY_LEFT: MoveCursor(-1,0);
    KEY_DOWN: MoveCursor(0,1);
    KEY_UP: MoveCursor(0,-1);
    KEY_EQUAL,KEY_KPPLUS : if scale<scaleMax[currentItemType] then begin   
        inc(scale);
        config.zoom[currentItemType]:=scale;
        StoreConfig;
    end;
    KEY_MINUS,KEY_KPMINUS : if scale>3 then begin
        dec(scale);
        config.zoom[currentItemType]:=scale;
        StoreConfig;
    end;
    KEY_G : begin
        config.showGrid := not config.showGrid;
        StoreConfig;
    end;
    KEY_DOT: begin
        UpdateItem;
        currentItemId := currentItemId + 1;
        if currentItemId >= gfxHeader.counts[currentItemType] then currentItemId := 0;
        LoadItem;
        reload := true;        
    end;
    KEY_COMMA: begin
        UpdateItem;
        currentItemId := currentItemId - 1;
        if currentItemId >= gfxHeader.counts[currentItemType] then currentItemId := gfxHeader.counts[currentItemType] - 1;
        LoadItem;
        reload := true;
    end;
    
    KEY_A..KEY_F: ColorKeyPressed(key.code + 6);
    KEY_1..KEY_9: ColorKeyPressed(key.code - 29);
    KEY_0: ColorKeyPressed(0);
    
    KEY_INSERT: begin   
        DrawPixel(cursorX,cursorY,currentColor);
        cursorMode := CUR_KEY;
    end;

    KEY_PAGEUP: ColorShift(key.shift,1);
    
    KEY_PAGEDOWN: ColorShift(key.shift,-1);

    KEY_X: SwapColBack;

    KEY_BACKSPACE: if Confirm('Clear') then ClearBoard(currentBackground);

    KEY_H: if key.ctrl then begin SaveUndoCmd(UNDO_FLIPH); FlipH; end;

    KEY_V: if key.ctrl then begin SaveUndoCmd(UNDO_FLIPV); FlipV; end;

    KEY_R: if key.ctrl then begin 
                if key.shift then SaveUndoCmd(UNDO_ROTCW) else SaveUndoCmd(UNDO_ROTCCW); 
                Rotate(not key.shift);
            end else SelectTool(TOOL_RECT);

    KEY_P: if key.ctrl then begin   
                if key.shift then ChooseBackground(GetPixelColor(cursorX,cursorY)) 
                    else ChooseColor(GetPixelColor(cursorX,cursorY));
            end else SelectTool(TOOL_DRAW);

    KEY_S: if not key.ctrl then SelectTool(TOOL_SHIFT);
    KEY_I: SelectTool(TOOL_FILL);

    KEY_GRAVE: if key.shift then BrushSet(brushSize-1) else BrushSet(brushSize+1);

    KEY_F7: EditColor(currentColor);

    KEY_Z: if key.ctrl then Undo;

    KEY_Y: if key.ctrl then Redo;

end;                    

if drawStart and (mouse.but=0) and (key.code=0) then begin
    SaveUndoCanvas;
    drawStart := false;
end;



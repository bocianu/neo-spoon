procedure ToggleHelp;
begin
    if helpScreen<>$ff then begin // close 
        currentTab:=helpScreen;
        helpScreen:=$ff;
    end else begin // open
        helpScreen:=currentTab;
        currentTab:=$ff;
    end;
    reload:=true;
end;

procedure ShowHelp(page:byte);
var helpline:byte;
helpFile: pchar = 
'File shortcuts are available on all tabs:'#0#0+
'ctrl+N : create a new tilemap'#0+
'ctrl+O : open the timemap file'#0+
'ctrl+S : save the current tilemap to a file'#0+
'ctrl+D : save the current tilemap as a new file'#0#0+
'To switch between tabs, you can use the TAB'#0+
'or shift+TAB key, or the F1-F4 keys.'#0+
'The "?" key allows you to get contextual'#0+
'help about the current tab contents/shortcuts.'#0#0+
'F10 : switches to "Spoon editor" app (if present).'#0#$ff;

helpTiles: pchar = 
'The tab contains list of the tiles.'#0+
'To select an item for drawing, click on it,'#0+
'or select it with the cursor keys.'#0#0+
'Right-click to select background tile.'#0#0+
'Keyboard shortcuts:'#0+
'ctrl+L : load tiles (gfx file)'#0+
'ctrl+P : load colour polette'#0+
'B : select current tile as background'#0#$ff;

helpMap: pchar = 
'Edit window keyboard shortcuts:'#0#0+
'SPACE/DELETE : place/delete a tile'#0+
'</> - PgUp/PgDn : switch to previous/next tile'#0+
'BACKSPACE : clear the map with selected back tile'#0+
'G : toggle grid'#0+
'ctrl+P/wheel click : selects a tile from the map'#0+
'shift + arrows : shift tileMap'#0#0+
'To scroll tileMap, hold mouse cursor over the edge'#0+
'of the map, or move the cursor out of the screen'#0+
'using cursor keys.'#0#0+
'Home / End : moves view to the map edges'#0#$ff;

helpConfig: pchar = 
'The application configuration is automatically'#0+
'saved to the ".tspoon.cnf" file in the current'#0+
'program directory.'#0#0+
'You can use mouse wheel to change map height/width'#0#0+
'ctrl+R : restore default configuration'#0#$ff;

procedure WriteHelpLine;
begin
    NeoDrawString(6,helpline,tmpstr);
    inc(helpline,10);
end;

procedure WriteHelp(p:pchar);
var i:word;
    len:byte absolute tmpstr;
begin
    i:=0;
    len:=0;
    while p[i]<>#$ff do begin
        if p[i]=#0 then begin   
            WriteHelpLine;
            len:=0;
        end else begin
            inc(len);
            tmpstr[len]:=p[i];
        end;
        inc(i);
    end;
end;

begin
    NeoSetColor(GUI_BACKDARK);
    NeoDrawLine(4,26,316,26);
    NeoSetColor(GUI_TEXTBOLD);
    NeoSetSolidFlag(0);
    tmpstr:='Help Page :';
    NeoDrawString(6,16,tmpstr);
    tmpstr:=tabnames[page];
    NeoDrawString(78,16,tmpstr);
    NeoSetColor(GUI_TEXT);
    helpline:=30;
    case page of
        0: WriteHelp(helpFile);
        1: WriteHelp(helpMap);
        2: WriteHelp(helpTiles);
        3: WriteHelp(helpConfig);
    end;
    tmpstr:='Press ESC to return to the program.';
    BottomBar(GUI_TEXT,GUI_BOTTOM,tmpstr);
end;


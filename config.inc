type TConfig =   record
    noWarnMode: boolean;
    showGrid: boolean;
    wrapImage: boolean;
    drawOnColorKey: boolean;
    zoom: array [0..2] of byte;
end;

var config: TConfig;

procedure StoreConfig;
begin
    NeoSave(CONFIG_FILE,word(@config),SizeOf(TConfig));
    reload := true;
end;

procedure DefaultConfig;
begin
    config.noWarnMode := false;
    config.showGrid := true;
    config.wrapImage := true;
    config.drawOnColorKey := false;
    config.zoom[0] := 8;
    config.zoom[1] := 8;
    config.zoom[2] := 4;
    StoreConfig;
end;

procedure InitConfig;
var a:byte;
    size:cardinal;
    loaded:boolean;
begin
    tmpfile := CONFIG_FILE;
    loaded:=false;
    if NeoStatFile(@tmpfile,size,a) then 
        if size=sizeOf(TConfig) then 
            loaded := NeoLoad(CONFIG_FILE,word(@config));
    if not loaded then DefaultConfig;
end;

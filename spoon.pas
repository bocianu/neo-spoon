uses crt,neo6502,neo6502math,neo6502keys;
const
{$i const.inc}
    VER = '0.90'; 
    CONFIG_FILE = '/.spoon.cnf';

{$i types.inc}

const   
    tabs:array [0..TAB_COUNT] of byte = (TAB_FILE,TAB_TILES,TAB_SPR16,TAB_SPR32,TAB_EDITOR,TAB_CONFIG,TAB_COLOREDIT);
    paletteTiles: array[0..15] of byte = (0,1,2,3,4,5,6,7,8,9,$a,$b,$c,$d,$e,$f);
    paletteSprites: array[0..15] of byte = (0,$10,$20,$30,$40,$50,$60,$70,$80,$90,$a0,$b0,$c0,$d0,$e0,$f0);
    tabnames:array [0..TAB_COUNT-1] of string[6] = ('File','Tiles','Spr16','Spr32','Editor','Config');
    typeWidths: array [0..2] of byte = (16,16,32);
    typeSizes: array [0..2] of word = (TILE_SIZE, SPR16_SIZE, SPR32_SIZE);
    typeCols: array [0..2] of byte = (I16_COLS,I16_COLS,I32_COLS);
    typeLimit: array [0..2] of byte = (128,128,64);
    typeBoxsize: array [0..2] of byte = (I16_BOXSIZE,I16_BOXSIZE,I32_BOXSIZE);
    typeLeft: array [0..2] of byte = (I16_OFF,I16_OFF,I32_OFF);
    typePalettes: array [0..2] of pointer = (@paletteTiles,@paletteSprites,@paletteSprites);
    imgOffset: array [0..2] of byte = (0,128,192);
    scaleMax: array [0..2] of byte = (11,11,5);
    hexChars:array [0..15] of char = ('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');

var 
    reload,reloadboard,quit:boolean;
    joyCount:byte;
    joyDelay:byte;
    inputDelay:byte;
    currentItemId:byte;
    currentItemType:byte;
    currentTab:byte;
    currentTabType:byte;
    currentItemSize:word;
    currentItemWidth:byte;
    currentTabItem:array [0..2] of SmallInt;
    currentColor:byte;
    currentTool:byte;
    currentPalette: array [0..0] of byte;
    currentBackground:byte;
    gfxHeader: TGfxHeader absolute GFX_HEADER_ADDRESS;
    canvas: array [0..0] of byte absolute CANVAS_ADDRESS;
    importHeader: TGfxHeader;
    tmpKey,key: TKey;
    okMessage,tmpstr:string[56];
    gfxfile,tmpfile:TString;
    frame:byte;
    delayedReload:word;
    scale:byte;
    cursorX,cursorY,cursorWidth,cursorHeight:byte;
    brushSize:byte;
    clipboardData:array [0..511] of byte;
    undoBuffer,undoOrigin:array [0..511] of byte;
    undoAddresses:array [0..UNDO_MAX-1] of word;
    undoFirstId:byte;
    undoLastId:byte;
    undoCount:byte;
    redoCount:byte;
    clipboardSize:word;
    itemPage:byte;
    itemStart:byte;
    itemLast:byte;    

    mArea:^TMouseArea;
    mouseDelay:byte;
    prevMouseY,prevMouseX:word;
    mButPrev:byte;
    mWheelPrev:byte;
    mouse:TMouse;
    drawStart:boolean;

    cursorMode:byte;
    helpScreen:byte;

    isPaletteSwapped:boolean;

{$i config.inc}
{$i helpers.inc}
{$i items.inc}
{$i undo.inc}
{$i gui.inc}
{$i help.inc}
{$i mouse_events.inc}
{$i mouse.inc}
{$r resources.rc}

begin

    // INIT app
    reload:=true;
    quit:=false;
    isPaletteSwapped:=false;
    drawStart:=false;
    currentTab:=0;
    inputDelay:=INPUT_DELAY;
    joyDelay:=0;
    joyCount:=0;
    currentColor:=0;
    clipboardSize:=0;
    brushSize:=1;
    mWheelPrev:=0;
    cursorMode:=CUR_KEY;
    helpScreen:=$ff;
    currentItemType:=$ff;

    NeoSetChar(192,@charOff);
    NeoSetChar(193,@charOn);

    NeoResetPalette;
    if IsGfxDataLoaded then begin
        ReadHeader;
        if IsPaletteStored then RestorePalettes;
    end else NewSheet;
    RefreshGuiPalette;

    InitConfig;
    
    NeoSetDefaults(0,0,1,1,0);
    NeoSelectCursor(0);
    
    repeat                                          // MAIN LOOP
        NeoShowCursor(cursorMode shr 1);
        NeoWaitForVblank;
        BlinkCursor;

        if delayedReload = 1 then reload:=true;     
        if delayedReload > 0 then Dec(delayedReload);

                            // screen contents drawing


        if reloadboard and not reload then begin
            case currentTab of
                1,2,3:DrawElements(currentTab-1);                
                4:DrawBoard;                
            end;
            reloadboard := false;
        end;

        if reload then begin

            while keypressed do ReadKey; // clear key buffer

            cursorWidth:=brushSize;
            cursorHeight :=brushSize;

            if ((currentTab=2) or (currentTab=3)) xor isPaletteSwapped then SwapPalettes; // swap palette for sprite tabs

            NeoSetColor(GUI_BACK);
            NeoSetSolidFlag(1);
            NeoDrawRect(0,0,319,239); // clear
            
            ShowMainScreen;

            if helpScreen=$ff then begin
                case currentTab of
                    0:ShowFileTab;
                    1,2,3:ShowItemsTab(currentTab-1);                
                    4:ShowEditorTab;     
                    5:ShowConfigTab;           
                end;
            end else ShowHelp(helpScreen);

            if okMessage<>'' then begin
                OK(okMessage);
                okMessage:='';
            end;

            reload := false;
        end;


        if inputDelay > 0 then begin
            Dec(inputDelay);
            mouse.butchange:=0;
            key.code:=0;
            mouse.but:=0;
            mouse.wheel:=0;
        end else GetUserInput;  // reads all inputs returning the user action in key and mouse record

        // interact on tabs

        if (currentTab = 1) or (currentTab = 2) or (currentTab = 3) then begin      // ITEM TAB
            {$i actions_list.inc}
        end;

        if (currentTab = 4) and (currentItemType<>$ff) then begin                   // EDITOR TAB
            {$i actions_editor.inc}
        end;

        if currentTab=5 then begin                                                  // CONFIG TAB
            {$i actions_config.inc}
        end;

        {$i actions_all.inc}

    until quit;
    
    while keypressed do readkey;
    NeoShowCursor(0);
    NeoResetPalette;
    ClrScr;

end.


if mouse.LDown then 
    case mouse.areaId of
        17:key.code := KEY_D;
        18:key.code := KEY_G;
        19:key.code := KEY_W;
        20:key.code := KEY_C;
    end;

case key.code of
    KEY_R: if key.ctrl then begin // ctrl R
        DefaultConfig;
        reload := true;
    end;
    KEY_D: begin
        config.noWarnMode := not config.noWarnMode;
        StoreConfig;
    end;
    KEY_G: begin
        config.showGrid := not config.showGrid;
        StoreConfig;
    end;
    KEY_W: begin
        config.wrapImage := not config.wrapImage;
        StoreConfig;
    end;
    KEY_C: begin
        config.drawOnColorKey := not config.drawOnColorKey;
        StoreConfig;
    end;
end;

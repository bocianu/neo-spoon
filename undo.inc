
procedure ClearUndos;
begin
    Fillbyte(@undoAddresses,UNDO_MAX*2,0);
    undoAddresses[0]:=UNDO_ADDRESS;
    undoFirstId:=0;
    undoLastId:=0;
    undoCount:=0;
end;

procedure ShiftUndoFirst;
begin
    if undoCount>0 then begin
        undoFirstId:=(undoFirstId + 1) and (UNDO_MAX-1);
        Dec(undoCount);
    end;
end;

procedure StoreToUndoOrigin;
begin
    WaitForBlitterFree;
    NeoBlitterCopy(CANVAS_ADDRESS,word(@undoOrigin),currentItemSize);
end;

function XorToUndoOrigin:boolean;
var i:word;
begin
    result:=false;
    for i:=0 to currentItemSize-1 do begin
        undoOrigin[i] := undoOrigin[i] xor canvas[i];
        if undoOrigin[i]<>0 then result:=true;
    end;
end;

procedure XorToCanvas;
var i:word;
begin
    for i:=0 to currentItemSize-1 do canvas[i] := undoOrigin[i] xor canvas[i];
end;

procedure SaveUndoData(usize:word);
var undoNextId:byte;
    nextAddress,i:word;
begin
    undoNextId := (undoLastId + 1) and (UNDO_MAX-1);
    if undoNextId = undoFirstId then ShiftUndoFirst;
    nextAddress := undoAddresses[undoLastId]; 
    i := 0;
    while usize > 0 do begin
        poke(nextAddress,undoBuffer[i]);
        Inc(nextAddress);
        if nextAddress = (UNDO_ADDRESS + UNDO_SIZE) then nextAddress := UNDO_ADDRESS;
        if nextAddress = undoAddresses[undoFirstId] then ShiftUndoFirst;
        Inc(i);
        Dec(usize);
    end;
    undoAddresses[undoNextId] := nextAddress;
    undoLastId := undoNextId;
    Inc(undoCount);
    redoCount := 0;
    //gotoxy(1,1); Write(HexStr(nextAddress,4),' ',HexStr(i,4),' ',undoCount,' ');
end;

procedure SaveUndoCanvas;
var usize:word;
begin
    if not XorToUndoOrigin then exit; // no difference - exit
    usize := PackRLE(word(@undoOrigin),currentItemSize,word(@undoBuffer),511);
    if ((usize = 0) or (usize >= currentItemSize)) then begin
        move(@canvas,@undoBuffer,currentItemSize);
        usize:=currentItemSize;
    end;
    SaveUndoData(usize)
end;

procedure SaveUndoCmd(cmd:byte);
begin
    undoBuffer[0]:=cmd;
    SaveUndoData(1);
end;

procedure UndoCommand(cmd:byte;isRedo:boolean);
begin
    case cmd of
        UNDO_FLIPH: FlipH;
        UNDO_FLIPV: FlipV;
        UNDO_SHIFTL: if isRedo then ShiftH(1) else ShiftH(-1);
        UNDO_SHIFTR: if isRedo then ShiftH(-1) else ShiftH(1);
        UNDO_SHIFTU: if isRedo then ShiftV(1) else ShiftV(-1);
        UNDO_SHIFTD: if isRedo then ShiftV(-1) else ShiftV(1);
        UNDO_ROTCW: Rotate(true xor isRedo);
        UNDO_ROTCCW: Rotate(false xor isRedo);
    end;
end;

procedure RestoreUndo(currAddress,nextAddress:word;isRedo:boolean);
var usize,i:word;
begin
    if nextAddress>currAddress then 
        usize := nextAddress - currAddress
    else 
        usize := nextAddress + UNDO_SIZE - currAddress;
    i:=0;
    reload:=true;
    if usize = 1 then begin
        UndoCommand(peek(currAddress),isRedo);
        exit;
    end;
    while usize > 0 do begin
        undoBuffer[i]:=peek(currAddress);
        Inc(currAddress);
        if currAddress = (UNDO_ADDRESS + UNDO_SIZE) then currAddress := UNDO_ADDRESS;
        Inc(i);
        Dec(usize);
    end;
    if i=currentItemSize then move(@undoBuffer,@canvas,i)
    else begin  
        ExpandRLE(word(@undoBuffer),word(@undoOrigin));
        XorToCanvas;
    end;
end;

procedure Undo;
var nextAddress:word;
begin
    if undoCount>0 then begin
        nextAddress := undoAddresses[undoLastId];
        undoLastId := (undoLastId - 1) and (UNDO_MAX-1);
        RestoreUndo(undoAddresses[undoLastId],nextAddress,false);
        Dec(undoCount);
        Inc(redoCount);
    end;
end;

procedure Redo;
var currAddress:word;
begin
    if redoCount>0 then begin
        currAddress := undoAddresses[undoLastId];
        undoLastId := (undoLastId + 1) and (UNDO_MAX-1);
        RestoreUndo(currAddress,undoAddresses[undoLastId],true);
        Dec(redoCount);
        Inc(undoCount);
    end;
end;


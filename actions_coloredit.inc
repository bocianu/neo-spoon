

if mouse.LDown then begin  
    case mouse.areaId of
        21..23: refresh := SliderClick;
        24: key.code := KEY_ENTER;
        25: key.code := KEY_ESC;
    end;
end;

if (mouse.butchange = 0) and (mouse.but and 1 <> 0) then begin // button L pressed down (continuos event)
    case mouse.areaId of
        21..23: refresh := SliderClick;
    end;
end;

if mouse.wheel <> 0 then begin
    mouse.wheel := mouse.wheel * $f;
    case mouse.areaId of
        21..23: begin crgb[mouse.areaId-21]:=crgb[mouse.areaId-21]-mouse.wheel; refresh:=true; end;
    end;
end;

case key.code of

    KEY_ESC: begin
        NeoSetPalette(col,pr,pg,pb);
        done := true;
    end;

    KEY_ENTER: done:=true;

    KEY_R: begin    
        if key.shift then crgb[0]:=crgb[0]-$f else crgb[0]:=crgb[0]+$f;
        refresh := true;
    end;

    KEY_G: begin    
        if key.shift then crgb[1]:=crgb[1]-$f else crgb[1]:=crgb[1]+$f;
        refresh := true;
    end;

    KEY_B: begin    
        if key.shift then crgb[2]:=crgb[2]-$f else crgb[2]:=crgb[2]+$f;
        refresh := true;
    end;

end;

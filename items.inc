function GetItemAddress(itype,id:byte): word;
var t: shortInt;
begin
    result := $100;
    for t:=0 to itype-1 do
        result := result + (typeSizes[t] * gfxHeader.counts[t]);
    result := result + (id * typeSizes[itype]);
end;

procedure UpdateItem;
begin
    if currentItemType<>$ff then MoveR2G(CANVAS_ADDRESS,GetItemAddress(currentItemType,currentItemId),currentItemSize);
end;

procedure LoadItem;
begin
    if currentItemType<>$ff then MoveG2R(GetItemAddress(currentItemType,currentItemId),CANVAS_ADDRESS,currentItemSize);
end;

function GFXSave(var fname:string):boolean;
var size:word;
    dlen,off:word;
begin
    UpdateItem;
    size:=GetDataSize;
    result := false;
    if size=$100 then exit(false); // empty 
    if NeoOpenFile(0,@fname,OPEN_MODE_NEW) then begin
        off:=0;
        result:=true;
        while size>0 do begin
            dlen:=ITEM_BUFFER_SIZE;
            if size<dlen then dlen := size;
            MoveG2R(off,CANVAS_ADDRESS,dlen);
            if NeoWriteFile(0,CANVAS_ADDRESS,dlen)<>dlen then result:=false;
            Inc(off,dlen);
            Dec(size,dlen);
        end;
        if not NeoCloseFile(0) then result:=false;
    end;
    LoadItem;
end;

function AddItem(itype:byte;pos:byte):boolean;
var src,size: word;
begin
    size := GetDataSize;
    if size+typeSizes[itype]>SPRITES_MAXSIZE then
        begin
            Error('Memory limit reached.');
            exit(false);
        end;
    if gfxHeader.counts[itype]=typeLimit[itype] then
        begin
            Error('Item limit reached.');
            exit(false);
        end;
    src := GetItemAddress(itype, pos);
    size := size - src;
    // move items data forward to make place for a new item
    if size>0 then MoveG2G(src-typeSizes[itype],src,size+typeSizes[itype]);
    FillGByte(src,typeSizes[itype],$0);
    Inc(gfxHeader.counts[itype]);
    StoreHeader;
    reload := true;
    result := true;
end;

procedure DelItem(itype:byte;pos:byte);
var src,size: word;
begin
    if pos >= gfxHeader.counts[itype] then exit;
    size := GetDataSize;
    src := GetItemAddress(itype, pos) + typeSizes[itype];
    size := size - src;
    if size>0 then MoveG2G(src,src-typeSizes[itype],size);
    Dec(gfxHeader.counts[itype]);
    StoreHeader;
    reload := true;
end;

procedure GetCopy(itype:byte;pos:byte);
var src: word;
begin
    if pos >= gfxHeader.counts[itype] then exit;
    src := GetItemAddress(itype, pos);
    moveG2R(src,word(@clipboardData),typeSizes[itype]);
    clipboardSize := typeSizes[itype];
    OK('Image copied to clipboard');
end;

procedure PasteCopy(itype:byte;pos:byte);
var src: word;
begin
    if pos >= gfxHeader.counts[itype] then 
        begin
            //Error('Out of range.');
            exit;
        end;
    if clipboardSize=0 then begin
            //Error('Clipboard empty.');
            exit;
        end;
    if clipboardSize<>typeSizes[itype] then
        begin
            Error('Clipboard data size mismatch.');
            exit;
        end;
    src := GetItemAddress(itype, pos);
    moveR2G(word(@clipboardData),src,clipboardSize);
    reload := true;
    //UpdateItem;
end;

procedure ClearBoard(col:byte);
var nn: byte;
begin
    StoreToUndoOrigin;
    nn := col shl 4;
    nn := nn or col;
    FillByte(@canvas,currentItemSize,nn);
    reload := true;
    UpdateItem;
    SaveUndoCanvas;
end;

function GetPixelColorAddress(x,y:byte):word;
begin
    if currentItemType=2 then result := y shl 4
    else result := y shl 3;
    result := result + (x shr 1);
    result := result + CANVAS_ADDRESS;
end;

procedure PokePixel(x,y,col:byte);
var addr: word;
    mask,colbyte: byte;
begin
    mask := $f0;
    colbyte := col;
    addr := GetPixelColorAddress(x,y);
    if x and 1 = 0 then
        begin
            mask := mask shr 4;
            colbyte := colbyte shl 4;
        end;
    poke(addr, (peek(addr) and mask) or colbyte);
end;

procedure SetPixel(x,y,col:byte);
begin
    PokePixel(x,y,col);
    reloadboard := true;
end;

function GetPixelColor(x,y:byte): byte;
var addr: word;
begin
    addr := GetPixelColorAddress(x,y);
    if x and 1 = 1 then result := peek(addr) and $0f
    else result := (peek(addr) shr 4) and $0f;
end;

procedure FlipV;
var r,w: byte;
    addr0,addr1: word;
begin
    w := currentItemWidth shr 1;
    addr0 := CANVAS_ADDRESS;
    addr1 := addr0 + currentItemSize - w;
    for r:=0 to w-1 do
        begin
            move(pointer(addr0),@tmpstr,w);
            move(pointer(addr1),pointer(addr0),w);
            move(@tmpstr,pointer(addr1),w);
            inc(addr0,w);
            dec(addr1,w);
        end;
    reloadboard := true;
end;

procedure FlipH;
var y,w,b,x: byte;
begin
    w := currentItemWidth;
    for y:=0 to w-1 do
        begin
            for x:=0 to (w shr 1)-1 do
                begin
                    b := GetPixelColor(x,y);
                    SetPixel(x,y,GetPixelColor(w-1-x,y));
                    SetPixel(w-1-x,y,b);
                end;
        end;
    reloadboard := true;
end;

procedure Rotate(clockwise:boolean);
var y,w,x: byte;
    corners: array [0..6] of byte;
    coord: array [0..4] of byte; 
    o,c:byte;
begin
    w := currentItemWidth - 1;
    for x:=0 to ((w + 1) shr 1) - 1 do
        begin
            for y:=x to w - x - 1 do
                begin
                    coord[0] := x;
                    coord[1] := y;
                    coord[2] := w - x;
                    coord[3] := w - y;
                    for c:=0 to 3 do 
                        corners[c]:=GetPixelColor(coord[(c+3) and 3],coord[c]);
                    o:=0;
                    if clockwise then o:=2;
                    for c:=0 to 3 do 
                        SetPixel(coord[c],coord[(c+1) and 3],corners[(c+o) and 3]); 
           
                end;
        end;
end;

procedure ShiftH(d:shortInt);
var x,y,b,w,sx,ex:byte;
begin
    w:=currentItemWidth;
    if d>0 then begin
        sx:=0;
        ex:=w-1;
    end else begin
        sx:=w-1;
        ex:=0;
    end;
    for y:=0 to w-1 do begin
        b:=currentBackground;
        if config.wrapImage then b:=GetPixelColor(sx,y);
        x:=sx;
        repeat 
            SetPixel(x,y,GetPixelColor(x+d,y));
            x := x + d;
        until x = ex;
        SetPixel(x,y,b);
    end;
    reloadboard:=true;
    key.code:=0;    
end;

procedure ShiftV(d:shortint);
var r,w: byte;
    addr0: word;
begin
    w := currentItemWidth shr 1;
    d := d * w;
    addr0 := CANVAS_ADDRESS;
    if d<0 then addr0 := addr0 + currentItemSize - w;
    move(pointer(addr0),@tmpstr,w);
    for r:=0 to currentItemWidth-2 do
        begin
            move(pointer(addr0+d),pointer(addr0),w);
            addr0:=addr0+d;
        end;
    if config.wrapImage then move(@tmpstr,pointer(addr0),w)
        else fillbyte(pointer(addr0),w,(currentBackground shl 4) and currentBackground);
    reloadboard := true;
    key.code:=0;    
end;

procedure Fill(x,y: byte);
var bc: byte;
    sptr: word;
    stackx:array [0..FILL_BUFFER_SIZE] of byte;
    stacky:array [0..FILL_BUFFER_SIZE] of byte;
    lx,sx,sy:byte;

procedure FPush(x,y:byte);
begin
    stackx[sptr] := x;
    stacky[sptr] := y;
    sptr := (sptr + 1) and FILL_BUFFER_SIZE;
end;

procedure FPull(var x:byte;var y:byte);
begin
    x := stackx[sptr-1];
    y := stacky[sptr-1];
    sptr := (sptr - 1) and FILL_BUFFER_SIZE;
end;

function Inside(x,y:byte):boolean;
begin
    result := false;
    if x>=currentItemWidth then exit(false);
    if y>=currentItemWidth then exit(false);
    if GetPixelColor(x,y)=bc then result:=true;
end;

procedure Scan(lx,rx,y:byte);
var sa:boolean;
    x:byte;
begin
    sa := false;
    for x:=lx to rx do begin
        if not Inside(x,y) then sa := false
            else if not sa then begin
                FPush(x,y);
                sa := true;
            end;
    end;
end;

begin
    StoreToUndoOrigin;
    sptr:=0;
    bc := GetPixelColor(x,y);
    if bc = currentColor then exit;
    FPush(x,y);
    while (sptr>0) do begin
        FPull(sx,sy);
        lx := sx;
        while Inside(lx-1,sy) do begin
            SetPixel(lx-1,sy,currentColor);
            dec(lx);
        end;
        while Inside(sx,sy) do begin
            SetPixel(sx,sy,currentColor);
            inc(sx);
        end;
        Scan(lx,sx-1,sy+1);
        Scan(lx,sx-1,sy-1);
    end;
    SaveUndoCanvas;
end;

procedure NewSheet;
begin
    FillGByte(0,SPRITES_MAXSIZE,0);
    gfxHeader.format := 1;
    gfxHeader.counts[0] := 0;
    gfxHeader.counts[1] := 0;
    gfxHeader.counts[2] := 0;
    currentTabItem[0] := 0;
    currentTabItem[1] := 0;
    currentTabItem[2] := 0;
    currentItemType := $ff;
    reload := true;
    StoreHeader;
end;

procedure NextPage;
begin
    if gfxHeader.counts[currentTabType]>31 then begin
        itemPage := itemPage xor 1;
        currentTabItem[currentTabType] := itemPage * 32;
        reload := true;
    end;
end;

procedure ItemNext;
begin
    currentTabItem[currentTabType] := currentTabItem[currentTabType] + 1;
    if currentTabItem[currentTabType] >= gfxHeader.counts[currentTabType] then currentTabItem[currentTabType] := 0;    
    ClearUndos;
    reloadboard := true;
end;

procedure ItemPrev;
begin
    currentTabItem[currentTabType] := currentTabItem[currentTabType] - 1;
    if currentTabItem[currentTabType] < 0 then currentTabItem[currentTabType] := gfxHeader.counts[currentTabType] - 1;
    ClearUndos;
    reloadboard := true;
end;


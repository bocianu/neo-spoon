procedure SetTile(x,y,t:byte);
begin
    tileMap[y*tileMapHeader.width+x] := t;
    reloadboard := true;
end;

function GetTile(x,y:byte):byte;
begin
    result := tileMap[y*tileMapHeader.width+x];
end;

procedure NewSheet;
begin
    FillByte(@gfxHeader,$100,0);
    gfxHeader.format := 1;
    reload := true;
    StoreHeader;
end;

procedure saveMapTmp;
begin
    //tmpfile := '.map.tmp';
    NeoSave(TMPMAP_FILE,TILEMAP_HEADER,tileMapSize+3)
end;

procedure NewMap;
begin
    if config.mapWidth * config.mapHeight > TILEMAP_MAXSIZE then begin
        config.mapWidth:=20;
        config.mapHeight:=13;
    end;
    tileMapSize := config.mapWidth * config.mapHeight;
    tileMapHeader.format := 1;
    tileMapHeader.width := config.mapWidth;
    tileMapHeader.height := config.mapHeight;
    FillByte(@tileMap,tileMapSize,currentBackground);
    saveMapTmp;
end;

function LoadMapFile(var fname:string):boolean;
var size:cardinal;
    msize: word;
    attr:byte;
begin
    result:=false;
    if not NeoStatFile(@fname,size,attr) then exit(false);
    if size>TILEMAP_MAXSIZE+3 then exit(false);
    if not NeoOpenFile(0,@fname, OPEN_MODE_RO) then exit(false);
    attr := NeoReadFile(0,word(@tileMapHeader),3);
    if attr<>3 then exit(false);
    if tileMapHeader.format <> 1 then exit(false);
    tileMapSize := tileMapHeader.width * tileMapHeader.height;
    size := NeoReadFile(0,word(@tileMap),tileMapSize);
    if size = tileMapSize then result:=true;
    NeoCloseFile(0);
    config.mapHeight := tileMapHeader.height;
    config.mapWidth := tileMapHeader.width;
end;



function LoadMapTmp:boolean;
var size:cardinal;
    attr:byte;
begin
    result := false;
    tmpfile := TMPMAP_FILE;
    if NeoStatFile(@tmpfile,size,attr) then 
        result := LoadMapFile(tmpfile);
end;

procedure TileNext;
begin
    currentTile := currentTile + 1;
    if (currentTile >= gfxHeader.counts[0]) and (currentTile<$f0) then currentTile := $f0;    
    //ClearUndos;
    reloadboard := true;
end;

procedure TilePrev;
begin
    currentTile := currentTile - 1;
    if currentTile = $ef then currentTile := gfxHeader.counts[0] - 1;
    //ClearUndos;
    reloadboard := true;
end;

procedure SolidsClick;
var clickId:byte;
begin
    clickId := (mouse.x div 20) or $f0;
    if mouse.RDown then currentBackground := clickId;
    if mouse.LDown then currentTile:=clickId;
    if mouse.dblclick > 0 then key.code := KEY_F2;;
    mouseDelay:=30;
    reloadboard:=true;
end;

procedure TilesClick;
var clickId:byte;
begin
    clickId := (mouse.x div 20) + (((mouse.y - 15) div 20) * 16);
    if clickId<gfxHeader.counts[0] then begin
        if mouse.RDown then currentBackground := clickId;
        if mouse.LDown then currentTile:=clickId;
        if mouse.dblclick > 0 then key.code := KEY_F2;;
        mouseDelay:=30;
        reloadboard:=true;
    end;
end;

function getClickPos:boolean;
begin
    clickedTileX := word(mouse.x + tileMapXoff) div 16;
    clickedTileY := ((mouse.y + tileMapYoff)-TMAP_TOP) div 16;
    result := (clickedTileX < tileMapHeader.width) and (clickedTileY < tileMapHeader.height);
end;

procedure CanvasClick;
begin
    if getClickPos then begin
        cursorX:=clickedTileX;
        cursorY:=clickedTileY;
        reloadboard:=true;
    end;
end;

procedure CanvasMiddleClick;
begin
    if getClickPos then begin
        currentTile := GetTile(clickedTileX,clickedTileY);
        reloadboard := true;
    end;
end;

procedure CanvasPressed;
begin
    if getClickPos then begin
        cursorX:=clickedTileX;
        cursorY:=clickedTileY;
        if mouse.but and 1 <> 0 then SetTile(cursorX,cursorY,currentTile);
        if mouse.but and 2 <> 0 then SetTile(cursorX,cursorY,currentBackground);
    end;
end;

procedure FixCursorView;  
var cx,cy:integer;
begin
    if cursorX>tileMapHeader.width-1 then cursorX:=tileMapHeader.width-1;
    if cursorY>tileMapHeader.height-1 then cursorY:=tileMapHeader.height-1;
    cx := cursorX * 16;
    cy := cursorY * 16;
    if cx < tileMapXoff then xOffTarget := cx else 
    if cx > tileMapXoff + 304 then xOffTarget := cx - 304;
    if cy < tileMapYoff then yOffTarget := cy else
    if cy > tileMapYoff + 192 then yOffTarget := cy - 192;
end;

procedure MoveMap;
var dx,dy:integer;
begin
    dx := xOffTarget - tileMapXoff;
    dy := yOffTarget - tileMapYoff;
    tileMapXoff := tileMapXoff + (Sgn(dx)*SCROLLSTEP);
    tileMapYoff := tileMapYoff + (Sgn(dy)*SCROLLSTEP);
    //FixCursorView;
    reload:=true;
end;

procedure ResizeIfNeeded;
var rsize,bsize,off:word;
    line:byte;
begin
    if (config.mapHeight <> tileMapHeader.height) or (config.mapWidth <> tileMapHeader.width) then begin
        if Confirm('Resize') then begin

            if config.mapHeight > tileMapHeader.height then begin
                bsize := (config.mapHeight - tileMapHeader.height) * tileMapHeader.width;
                FillByte(@tileMap[tileMapSize],bsize,currentBackground);
            end;
            tileMapHeader.height := config.mapHeight;
            tileMapSize := tileMapHeader.height * tileMapHeader.width;
            if config.mapWidth > tileMapHeader.width then begin // extend lines
                off := TILEMAP_ADDRESS + tileMapHeader.width;
                bsize := config.mapWidth - tileMapHeader.width;
                rsize := tileMapSize - tileMapHeader.width;
                for line:=1 to tileMapHeader.height do begin
                    move(pointer(off),pointer(off+bsize),rsize);
                    fillbyte(pointer(off),bsize,currentBackground);
                    Inc(off,config.mapWidth);
                    dec(rsize,tileMapHeader.width);
                end;
            end else                                          
            if config.mapWidth < tileMapHeader.width then begin // cut lines
                off := TILEMAP_ADDRESS + config.mapWidth;
                bsize := tileMapHeader.width - config.mapWidth;
                rsize := tileMapSize - tileMapHeader.width;
                for line := 2 to tileMapHeader.height do begin
                    move(pointer(off+bsize),pointer(off),rsize);
                    Inc(off,config.mapWidth);
                    dec(rsize,tileMapHeader.width);
                end;
            end;
            tileMapHeader.width := config.mapWidth;
            tileMapSize := tileMapHeader.height * tileMapHeader.width;

        end else begin
            config.mapWidth := tileMapHeader.width;
            config.mapHeight := tileMapHeader.height;
        end;
    end;
end;

procedure SwapColBack;
var b:byte;
begin
    b:=currentTile;
    currentTile := currentBackground;
    currentBackground := b;
    reload := true;
end;

procedure ValidateBrushes;
begin
    if (currentTile>=gfxHeader.counts[0]) and (currentTile<$f0) then currentTile := $ff;
    if (currentBackground>=gfxHeader.counts[0]) and (currentBackground<$f0) then currentBackground := $f0;
end;

procedure ShiftH(d:shortInt);
var x,y,b,w,sx,ex:byte;
begin
    w:=tileMapHeader.width;
    if d>0 then begin
        sx:=0;
        ex:=w-1;
    end else begin
        sx:=w-1;
        ex:=0;
    end;
    for y:=0 to tileMapHeader.height-1 do begin
        b:=currentBackground;
        b:=GetTile(sx,y);
        x:=sx;
        repeat 
            SetTile(x,y,GetTile(x+d,y));
            x := x + d;
        until x = ex;
        SetTile(x,y,b);
    end;
    reloadboard:=true;
    key.code:=0;    
end;

procedure ShiftV(d:shortint);
var r,w: byte;
    addr0: word;
    buf:array[0..255] of byte absolute $600;
begin
    w := tileMapHeader.width;
    d := d * w;
    addr0 := TILEMAP_ADDRESS;
    if d<0 then addr0 := addr0 + (tileMapHeader.height * w) - w;
    move(pointer(addr0),buf,w);
    for r:=0 to tileMapHeader.height-2 do
        begin
            move(pointer(addr0+d),pointer(addr0),w);
            addr0:=addr0+d;
        end;
    move(buf,pointer(addr0),w);
    reloadboard := true;
    key.code:=0;    
end;
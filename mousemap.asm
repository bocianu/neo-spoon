    icl 'const.inc'

;;          tab, x, y, w, h, eventID
    
    dta     TAB_EDITOR,a(BOARD_X0),a(BOARD_Y0),a(190),190,0  ; canvas

    // TABS
    dta     TAB_ALL,a(1),              a(0),   a(TAB_WIDTH),    11,     1  ; tab 0
    dta     TAB_ALL,a(TAB_WIDTH*1),    a(0),   a(TAB_WIDTH),    11,     2  ; tab 1
    dta     TAB_ALL,a(TAB_WIDTH*2),    a(0),   a(TAB_WIDTH),    11,     3  ; tab 2
    dta     TAB_ALL,a(TAB_WIDTH*3),    a(0),   a(TAB_WIDTH),    11,     4  ; tab 3
    dta     TAB_ALL,a(TAB_WIDTH*4),    a(0),   a(TAB_WIDTH),    11,     5  ; tab 4
    dta     TAB_ALL,a(TAB_WIDTH*5),    a(0),   a(TAB_WIDTH),    11,     6  ; tab 5

    // TILES SPRITES 16 

    dta     TAB_TILES+TAB_SPR16+TAB_SPR32,a(1),a(15),a(318),186,7 ; tiles / sprites list

    dta     TAB_ALL,a(312),a(0),a(8),11,8 ; help

    // editor
    dta     TAB_EDITOR,a(1),a(216),a(317),11,9 ; palette

    dta     TAB_EDITOR,a(216),a(63),a(48),12,10  ; POINT
    //dta     TAB_EDITOR,a(268),a(63),a(42),12,11  ; RECT
    dta     TAB_EDITOR,a(216),a(81),a(48),12,12  ; FILL
    dta     TAB_EDITOR,a(216),a(99),a(48),12,13  ; SHIFT

    dta     TAB_EDITOR,a(216),a(192),a(15),12,14  ; ^H
    dta     TAB_EDITOR,a(236),a(192),a(15),12,15  ; ^V
    dta     TAB_EDITOR,a(256),a(192),a(15),12,16  ; ^R

    // brush 
    dta     TAB_EDITOR,a(PANEL_LEFT+5),a(CINFO_TOP+24),a(25),8,21 ; brush


    // config
    dta     TAB_CONFIG,a(16),a(30),a(200),8,17   ; Disable
    dta     TAB_CONFIG,a(16),a(46),a(200),8,18   ; Grid
    dta     TAB_CONFIG,a(16),a(62),a(200),8,19   ; Wrap
    dta     TAB_CONFIG,a(16),a(78),a(200),8,20   ; color

    // RGB sliders

    dta     TAB_COLOREDIT,a(228),a(125),a(79),7,21  ; R
    dta     TAB_COLOREDIT,a(228),a(140),a(79),7,22  ; G
    dta     TAB_COLOREDIT,a(228),a(155),a(79),7,23  ; B

    dta     TAB_COLOREDIT,a(236),a(174),a(54),20,24  ; Change color
    dta     TAB_COLOREDIT,a(230),a(192),a(64),20,25  ; Cancel 

    dta     TAB_TILES+TAB_SPR16+TAB_SPR32,a(3),a(202),a(77),20,26  ; Import
    dta     TAB_TILES+TAB_SPR16+TAB_SPR32,a(84),a(202),a(77),20,27  ; Load Palette
    dta     TAB_TILES+TAB_SPR16+TAB_SPR32,a(166),a(202),a(77),20,28  ; Save Palette
    dta     TAB_TILES+TAB_SPR16+TAB_SPR32,a(252),a(202),a(60),20,29  ; Next Page


    dta     0 ;footer

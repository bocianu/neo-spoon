var
    charOff: array [0..6] of byte = 
    (
        %00000000,
        %11111100,
        %10000100, 
        %10000100,
        %10000100,
        %10000100,
        %11111100
    );
    charOn: array [0..6] of byte = 
    (
        %00000000,
        %11111100,
        %10000100, 
        %10110100,
        %10110100,
        %10000100,
        %11111100
    );

procedure RefreshGuiPalette;
begin
    //NeoResetPalette;
    NeoSetPalette(GUI_BLACK,0,0,0);            // black
    NeoSetPalette(GUI_TEXT,200,200,200);      // light grey - default text
    NeoSetPalette(GUI_BOTTOM,0,$30,$80);          // dark blue - bottom bar 
    NeoSetPalette(GUI_BACK,$50,$50,$50);         // dark grey - default back
    NeoSetPalette(GUI_CURSOR,250,100,120);      // blinking cursor
    NeoSetPalette(GUI_BACKDARK,$30,$30,$30);         // darker grey - inactive tab
    NeoSetPalette(GUI_BACKOK,0,$80,$10);         // green  - OK bar 
    NeoSetPalette(GUI_BACKERROR,$80,$10,0);          // red  - ERROR bar 
    NeoSetPalette(GUI_TEXTBOLD,240,220,100);      // yello  - light text
    NeoSetPalette(GUI_TOOL,40,120,220);      // selected tool bg
end;

procedure WaitForNoInput;
begin
    repeat 
        ReadKeyboard(key);
        key.code := key.code or NeoGetJoy(0);
    until key.code = 0;
end;


procedure GetUserInput;
begin
    if NeoIsMousePresent then ReadMouse;
    
    ReadKeyboard(key);

    if key.code = 0 then begin
        joyDelay := 0;
        joyCount := 0;
    end;
    
    if joyDelay = 0 then begin 
        if key.code <> 0 then begin 
            joyDelay := INPUT_DELAY;
            if joyCount > 0 then begin
                joyDelay := JOY_REPEAT;
            end else Inc(joyCount);
        end;
    end else begin  
        Dec(joyDelay);
        key.code := 0;
    end;
    
end;

procedure GetFileName(var s:string;hdr:TString);
var tstr:TString;
begin
    while keypressed do readkey;
    StorePalettes;
    NeoResetPalette;
    NeoSetTextColor(15,0);
    ClrScr;
    NeoShowDir;
    tstr:='';
    Writeln;
    Writeln(hdr);
    Writeln('Enter filename: ');
    Readln(tstr);
    move(@tstr,@s,Length(tstr)+1);
    RestorePalettes;
    RefreshGuiPalette;
    reload:=true;
end;

procedure DrawFrame(x:word;y,w,h,fc,bc:byte);
begin   
    if w>0 then dec(w);
    if h>0 then dec(h);
    NeoSetSolidFlag(1);
    NeoSetColor(bc);
    NeoDrawRect(x,y,x+w,y+h);
    NeoSetSolidFlag(0);
    NeoSetColor(fc);
    NeoDrawRect(x,y,x+w,y+h);
end;

procedure BottomBar(fc,bc:byte;var s:string);
begin
    NeoSetColor(GUI_BACKDARK);
    NeoSetSolidFlag(1);
    NeoDrawLine(0,228,319,228);
    NeoSetColor(bc);
    NeoDrawRect(0,229,319,239);
    NeoSetColor(GUI_BLACK);
    NeoSetSolidFlag(0);
    NeoDrawString(3,232,s);
    NeoSetColor(fc);
    NeoDrawString(2,231,s);
end;

procedure DrawButton(x:word;y:byte;fc:byte;var s:string);
var w:byte;
begin
    w:=(Length(s)*6)+6;
    DrawFrame(x,y,w,BUT_H,fc,GUI_BOTTOM);
    NeoSetColor(GUI_BLACK);
    NeoSetSolidFlag(0);
    NeoDrawString(x+4,y+4,s);
    NeoSetColor(GUI_TEXTBOLD);
    NeoDrawString(x+3,y+3,s);
end;

procedure ShowMainScreen;
var
    t:byte;
    x:word;
    col,bcol:byte;
begin
    NeoSetColor(GUI_BLACK);
    NeoSetSolidFlag(1);
    NeoDrawRect(0,0,319,10);            
    x:=0;
    for t:=0 to TAB_COUNT-1 do begin
        if (t=currentTab) or (t=helpScreen) then begin
            bcol:=GUI_BACK;
            col:=GUI_TEXT;
        end else begin
            col:=GUI_BACK;
            bcol:=GUI_BACKDARK;
        end;
        NeoSetColor(bcol);
        NeoSetSolidFlag(1);
        NeoDrawRect(x,0,x+TAB_WIDTH-2,10);            
        NeoSetColor(GUI_BLACK);
        NeoSetSolidFlag(0);
        tmpstr := tabnames[t];
        NeoDrawString(x+3,2,tmpstr);
        NeoSetColor(col);
        NeoDrawString(x+2,1,tmpstr);
        x:=x+TAB_WIDTH;
    end;
    if helpScreen=$ff then begin
        col:=GUI_BACK;
    end else begin
        col:=GUI_TEXTBOLD;
    end;
    NeoSetColor(col);
    tmpstr:='?';
    NeoDrawString(314,2,tmpstr);
end;

procedure SwitchTab(tnum:byte);
begin
    if tnum=$ff then tnum:=TAB_COUNT-1;
    if tnum=TAB_COUNT then tnum:=0;
    if helpScreen<>$ff then ToggleHelp;
    if tnum <> currentTab then begin
        if currentTab=4 then UpdateItem;
        currentTab := tnum;
        reload := true;
        inputDelay := INPUT_DELAY;
    end;
end;

procedure ShowConfigTab;
var 
    oy:byte;

procedure DrawOptBool(state:boolean;hotkey:byte;s:string[53]);
begin
    NeoSetSolidFlag(0);
    NeoSetColor(GUI_TEXT);
    if state then tmpstr:=#193 else tmpstr:=#192;
    tmpstr:=Concat(tmpstr,s);
    NeoDrawString(16,oy,tmpstr);            
    NeoSetColor(GUI_TEXTBOLD);
    tmpstr:=s[hotkey];
    NeoDrawString(16+(hotkey*6),oy,tmpstr);            
    Inc(oy,16);
end;

begin
    oy:=30;
    DrawOptBool(config.noWarnMode,2,' Disable Confirmations (YOLO mode)');
    DrawOptBool(config.showGrid,7,' Show Grid');
    DrawOptBool(config.wrapImage,2,' Wrap Image');
    DrawOptBool(config.drawOnColorKey,24,' Draw when you press a Color key');
   
    tmpstr := '^R:Restore Defaults';
    BottomBar(GUI_TEXT,GUI_BOTTOM,tmpstr);
end;

procedure ShowFileTab;
var p:word;
begin
    NeoSetColor(GUI_TEXTBOLD);
    NeoSetSolidFlag(0);
    tmpstr:=Concat('FileName: ', gfxfile);
    NeoDrawString(3,30,tmpstr);            
    NeoSetColor(GUI_TEXT);
    tmpstr:=MergeStrInt('Tiles: ', gfxHeader.counts[0]);
    NeoDrawString(3,50,tmpstr);
    tmpstr:=MergeStrInt('Sprites 16: ', gfxHeader.counts[1]);
    NeoDrawString(3,60,tmpstr);
    tmpstr:=MergeStrInt('Sprites 32: ', gfxHeader.counts[2]);
    NeoDrawString(3,70,tmpstr);
    tmpstr := MergeStrInt('data size: ', GetDataSize);
    tmpstr := Concat(tmpstr, ' bytes (');
    p:=(GetDataSize*100) div SPRITES_MAXSIZE;
    tmpstr := MergeStrInt(tmpstr, p);
    tmpstr := Concat(tmpstr, '%)');
    NeoDrawString(3,90,tmpstr);
    p:=(GetDataSize*311) div SPRITES_MAXSIZE;
    NeoSetColor(GUI_BLACK);
    NeoSetSolidFlag(1);
    NeoDrawRect(2,100,317,110);
    if p>0 then begin
        NeoSetColor(GUI_TEXT);
        NeoDrawRect(4,102,p+4,108);
    end;

    NeoSetColor(GUI_BACKDARK);
    NeoSetSolidFlag(0);
    tmpstr:='Spoon - Neo6502 asset editor';
    NeoDrawString(3,218,tmpstr);
    tmpstr:=Concat('version ', VER);
    NeoDrawString(243,218,tmpstr);
    
    tmpstr := '^N:New  ^O:Open  ^S:Save  ^D:Save as  TAB:Switch';
    BottomBar(GUI_TEXT,GUI_BOTTOM,tmpstr);

end;

procedure DrawElements(itype:byte);
var c,i,col,w:byte;
    x,y:word;

begin

    if itype=2 then begin
        c:=currentTabItem[itype] div 32;
        if c<>itemPage then begin
            itemPage:=c;
            reload:=true;
        end;
    end;

    w:=typeWidths[itype];
    if gfxHeader.counts[itype] > 0 then begin
        itemLast := gfxHeader.counts[itype]-1;
        itemStart := 32 * itemPage;
        c:=0;
        for i:=itemStart to itemLast do begin
            x:=(c mod typeCols[itype])*typeBoxsize[itype]+typeLeft[itype];
            y:=(c div typeCols[itype])*typeBoxsize[itype]+I_TOP;
            if i = currentTabItem[itype] then col:=20 else col:=0;

            NeoSetColor(0);
            NeoSetSolidFlag(1);
            NeoDrawRect(x-1,y-1,x+w,y+w);
            NeoSetColor(col);
            NeoSetSolidFlag(0);
            NeoDrawRect(x-1,y-1,x+w,y+w);
            NeoDrawImage(x,y,i+imgOffset[itype]);
            inc(c);
            if c=32 then break;
        end;
        NeoSetColor(GUI_BACK);
        NeoSetSolidFlag(1);
        NeoDrawRect(312-104,218,312,226);
        NeoSetColor(GUI_BACKDARK);
        NeoSetSolidFlag(0);
        tmpstr := MergeStrInt('element No:',currentTabItem[itype]);
        NeoDrawString(312-(Length(tmpstr)*6),218,tmpstr);
    end;
end;

procedure ShowItemsTab(itype:byte);
begin
    NeoStr(gfxHeader.counts[itype],tmpstr);
    tmpstr := concat(tmpstr,' elements - size: ');
    tmpstr := MergeStrInt(tmpstr,gfxHeader.counts[itype]*typeSizes[itype]);
    NeoSetColor(GUI_BACKDARK);
    NeoSetSolidFlag(0);
    NeoDrawString(3,218,tmpstr);

    if gfxHeader.counts[itype]=0 then begin
    tmpstr := 'Double-click anywhere to add a new item.';
    NeoDrawString(3,15,tmpstr);
    end;

    tmpstr:='Import items';
    DrawButton(3,202,GUI_BACKDARK,tmpstr);

    tmpstr:='Load Palette';
    DrawButton(84,202,GUI_BACKDARK,tmpstr);

    tmpstr:='Save Palette';
    DrawButton(165,202,GUI_BACKDARK,tmpstr);

    if (itype = 2) then begin
        if (gfxHeader.counts[itype] > 31) then begin
            tmpstr:='Next Page';
            if itemPage = 1 then tmpstr:='Prev Page'; 
            DrawButton(252,202,GUI_BACKDARK,tmpstr);
        end;
    end else itemPage := 0;
    
    tmpstr := '+:Add  INS:Insert  DEL:Delete  ^C:Copy  ^V:Paste';
    BottomBar(GUI_TEXT,GUI_BOTTOM,tmpstr);
    DrawElements(itype);
end;

procedure BlinkCursor;
begin
    Inc(frame);
    NeoSetPalette(GUI_CURSOR,frame shl 4,frame shl 2,frame shl 1);
end;

procedure ShowBrush;
var b,c,bx:byte;
begin
    bx:=PANEL_LEFT+8;
    for b:=1 to BRUSH_MAX_SIZE do begin
        c:=GUI_BLACK;
        if b=brushSize then c:=GUI_TEXT;
        NeoSetColor(c);
        NeoDrawRect(bx,CINFO_TOP+31-b,bx+b-1,CINFO_TOP+30);
        bx := bx + b + 3;
    end;
end;

procedure ShowCursorInfo;
begin
    NeoSetColor(GUI_BACKDARK);
    NeoSetSolidFlag(1);
    NeoDrawRect(244,CINFO_TOP,274,50);
    NeoSetColor(GUI_BACK);
    NeoSetSolidFlag(0);
    tmpstr := MergeStrInt('x:',cursorX);
    NeoDrawString(244,CINFO_TOP+2,tmpstr);
    tmpstr := MergeStrInt('y:',cursorY);
    NeoDrawString(244,CINFO_TOP+12,tmpstr);
    tmpstr := MergeStrInt('No:',currentItemId);
    NeoDrawString(244,CINFO_TOP+23,tmpstr);
    NeoSetSolidFlag(1);
end;

function Min(a,b:word):word;
begin
    if a < b then result := a else result := b;
end;

procedure DrawCursor(col:byte);
var px,py:byte;
    ex,ey:byte;
    ox:byte;
    s1:byte;
    bsize:byte;
begin
    s1:=scale+1;
    bsize:=s1*currentItemWidth-1;
    if config.showGrid then Dec(bsize);
    
    if cursorMode and CUR_KEY <> 0 then begin
        if currentTool<>TOOL_SHIFT then begin
            ox := 0;
            if config.showGrid then ox := 1;
            px := BOARD_X0 + cursorX * s1 - ox;
            py := BOARD_Y0 + cursorY * s1 - ox;
            ex := cursorWidth * s1 + ox - 1;
            ey := cursorHeight * s1 + ox - 1;
            NeoSetColor(col);
            NeoSetSolidFlag(0);
            NeoDrawRect(px,py,Min(px+ex,BOARD_X0+bsize),Min(py+ey,BOARD_Y0+bsize));
        end;
    end;
    ShowCursorInfo;
end;

procedure InitCursor;
begin
    cursorX:=0;
    cursorY:=0;
    cursorWidth:=brushSize;
    cursorHeight :=brushSize;
end;

procedure DrawCell(x,y,col:byte);
var c,csize:byte;
    px,py,ex,ey:byte;
    tx:word;
    ty:byte;
begin
    csize:=scale;
    c:=(scale+1);
    if config.showGrid then begin  
        Dec(csize);
    end;
    px:=BOARD_X0 + c*x;
    py:=BOARD_Y0 + c*y;
    tx:=310 - currentItemWidth + x;
    ty:=20 + y;
    ex:=px+csize;
    ey:=py+csize;
    NeoSetColor(currentPalette[col]);
    NeoSetSolidFlag(1);
    NeoDrawRect(px,py,ex,ey);
    NeoDrawPixel(tx,ty);
end;

procedure DrawPixel(x,y,col:byte);
var w,h:byte;
begin
    if not drawStart then begin
        drawStart := true;
        StoreToUndoOrigin;
    end;
    h:=cursorHeight;
    while h>0 do begin
        w:=cursorWidth;
        while w>0 do begin
            if (x<currentItemWidth) and (y<currentItemWidth) then begin
                PokePixel(x,y,col);
                DrawCell(x,y,col);
            end;
            inc(x);
            dec(w);
        end;
        inc(y);
        dec(x,cursorWidth);
        dec(h);
    end;
    DrawCursor(GUI_CURSOR);
    if key.code = 0 then key.code := KEY_NONE;
end;

procedure RedrawArea(x,y,w,h:byte);
var cursorWidth:byte;
begin
    while h>0 do begin
        cursorWidth:=w;
        while cursorWidth>0 do begin
            if (x<currentItemWidth) and (y<currentItemWidth) then DrawCell(x,y,GetPixelColor(x,y));
            inc(x);
            dec(cursorWidth);
        end;
        inc(y);
        dec(x,w);
        dec(h);
    end;
end;

procedure ClearCursor;
begin
    if config.showGrid then DrawCursor(GUI_BACK) else RedrawArea(cursorX,cursorY,cursorWidth,cursorHeight);
end;

procedure FixCursor;
begin
    if cursorX+cursorWidth-1=currentItemWidth then cursorX := 0;        
    if cursorX+cursorWidth>currentItemWidth then cursorX := currentItemWidth-cursorWidth;        
    if cursorY+cursorHeight-1=currentItemWidth then cursorY := 0;        
    if cursorY+cursorHeight>currentItemWidth then cursorY := currentItemWidth-cursorHeight;        
end;

procedure MoveCursor(dx,dy:byte);
begin
    cursorMode := cursorMode or CUR_KEY;
    ClearCursor;
    cursorX := cursorX + dx;
    cursorY := cursorY + dy;
    FixCursor;
    DrawCursor(GUI_CURSOR);
end;

procedure BrushSet(b:byte);
begin
    ClearCursor;
    brushSize:=b;
    if brushSize=0 then brushSize:=BRUSH_MAX_SIZE;
    if brushSize>BRUSH_MAX_SIZE then brushSize:=1;
    ShowBrush;
    cursorWidth:=brushSize;
    cursorHeight :=brushSize;
    FixCursor;
    DrawCursor(GUI_CURSOR);
end;

procedure DrawBoard;
var c,x,y,csize:byte;
    px,py,ex,ey:byte;
    tx:word;
    ty:byte;
    data:word;

    procedure DrawCell;
    begin
        ex:=px+csize;
        ey:=py+csize;
        NeoSetColor(currentPalette[c]);
        NeoDrawRect(px,py,ex,ey);
        NeoDrawPixel(tx,ty);
        px:=px+scale+1;
        inc(x);
        inc(tx);
    end;

begin
    ty:=20;
    csize:=scale;
    c:=(scale+1)*currentItemWidth;
    if config.showGrid then begin  
        Dec(csize);
        Dec(c);
    end;
    NeoSetColor(GUI_BACK);
    NeoSetSolidFlag(0);
    NeoDrawRect(BOARD_X0-1,BOARD_Y0-1,BOARD_X0+c,BOARD_Y0+c);
    py:=BOARD_Y0;
    data:=CANVAS_ADDRESS;
    NeoSetSolidFlag(1);
    for y:=0 to currentItemWidth-1 do begin
        px:=BOARD_X0;
        tx:=310-currentItemWidth;
        x:=0;
        while x<currentItemWidth do begin
            c:=peek(data) shr 4;
            DrawCell;
            c:=(peek(data) and $f);
            DrawCell;
            inc(data);
        end;
        py:=py+scale+1;
        Inc(ty);
    end;
    DrawCursor(GUI_CURSOR);
end;

procedure DrawPalette;
var
    c:byte;
    col:byte;
    x:word;
    
begin
    x:=1;
    NeoSetColor(GUI_BACKDARK);
    NeoSetSolidFlag(1);
    NeoDrawRect(0,PAL_Y-1,319,PAL_Y+12);
    for c:=0 to 15 do begin
        NeoSetSolidFlag(1);
        NeoSetColor(currentPalette[c]);
        NeoDrawRect(x,PAL_Y,x+17,PAL_Y+11);
        NeoSetColor(GUI_BLACK);
        NeoSetSolidFlag(0);
        tmpstr:=hexChars[c];
        NeoDrawString(x+7,PAL_Y+3,tmpstr);
        col:=GUI_TEXT;
        if c=currentColor then col:=GUI_CURSOR;
        NeoSetColor(col);
        NeoDrawString(x+6,PAL_Y+2,tmpstr);
        if c=currentBackground then begin
            NeoSetColor(GUI_CURSOR);
            NeoDrawRect(x,PAL_Y,x+17,PAL_Y+11);
        end;
        x:=x+20;
    end;
    DrawFrame(CURCOL_X+CURCOL_D,CURCOL_Y+CURCOL_D,CURCOL_W,CURCOL_W,GUI_BACK,currentPalette[currentBackground]);
    DrawFrame(CURCOL_X,CURCOL_Y,CURCOL_W,CURCOL_W,GUI_BACK,currentPalette[currentColor]);
end;



procedure PanelHR(y:byte);
begin
    NeoSetColor(GUI_BACK);
    NeoDrawLine(PANEL_LEFT,y,316,y);
end;

procedure ShowTool(x:word;y,tool:byte;s:string[8]);
var w:byte;
    bcol,tcol:byte;
begin
    x:=x+PANEL_LEFT;
    bcol := GUI_BOTTOM;
    tcol := GUI_TEXT;
    if (currentTool=tool) then begin
        bcol:=GUI_TOOL;
        tcol := GUI_TEXTBOLD;
    end;
    w:=(Length(s)*6)+6;
    DrawFrame(x,y,w,BUT_H,GUI_BACK,bcol);
    NeoSetColor(GUI_BLACK);
    NeoSetSolidFlag(0);
    NeoDrawString(x+4,y+4,s);
    NeoSetColor(tcol);
    NeoDrawString(x+3,y+3,s);
end;

procedure DrawTools;
begin
    ShowTool(6,63,TOOL_DRAW,'P:Point');
    ShowTool(6,81,TOOL_FILL,'I:Fill ');
    //ShowTool(58,63,TOOL_RECT,'R:Rect');
    ShowTool(6,99,TOOL_SHIFT,'S:Shift');
end;

procedure DrawSidePanel;
begin
    NeoSetSolidFlag(1);
    NeoSetColor(GUI_BACKDARK);
    NeoDrawRect(PANEL_LEFT,15,315,210);
    ShowBrush;
    PanelHR(56);
    DrawTools;
    PanelHR(176);
    NeoSetSolidFlag(0);
    NeoSetColor(GUI_TEXT);
    tmpstr := 'Transform';
    NeoDrawString(PANEL_LEFT+6,182,tmpstr);
    tmpstr := 'Pick';
    NeoDrawString(PANEL_LEFT+76,182,tmpstr);
    tmpstr := '^H';
    DrawButton(PANEL_LEFT+6,192,GUI_BACK,tmpstr);
    tmpstr := '^V';
    DrawButton(PANEL_LEFT+26,192,GUI_BACK,tmpstr);
    tmpstr := '^R';
    DrawButton(PANEL_LEFT+46,192,GUI_BACK,tmpstr);
    tmpstr := '^P';
    DrawButton(PANEL_LEFT+82,192,GUI_BACK,tmpstr);
end;

procedure ShowEditorTab;
begin
    if currentItemType<>$ff then begin
        DrawSidePanel;
        DrawBoard;
        tmpstr := '+-:Zoom  <>:Prev-Next  SPC:Draw  DEL:Erase  ESC:Back';
        BottomBar(GUI_TEXT,GUI_BOTTOM,tmpstr);
        DrawPalette;
    end else begin
        tmpstr := 'Select item to edit.';
        BottomBar(GUI_TEXT,GUI_BOTTOM,tmpstr);
    end;
end;

procedure ColorShift(shift:boolean;increment:byte);
begin
    if shift then currentBackground:=(currentBackground+increment) and 15
       else currentColor:=(currentColor+increment) and 15;
    DrawPalette;
end;

procedure Error(msg:TString);
begin
    BottomBar(GUI_TEXT,GUI_BACKERROR,msg);
    WaitForNoInput;
    repeat GetUserInput until key.code<>0;
    key.code:=0;
end;

procedure OK(msg:TString);
begin
    BottomBar(GUI_TEXT,GUI_BACKOK,msg);
    delayedReload := OK_MESSAGE_DELAY;
end;


procedure ShowColorPicker(col:byte);
begin
    DrawFrame(PANEL_LEFT,57,106,154,GUI_CURSOR,GUI_BACKDARK);
    NeoSetColor(GUI_TEXTBOLD);
    NeoSetSolidFlag(0);
    tmpstr:=MergeStrInt('Change color: ',col);
    NeoDrawString(PANEL_LEFT+6,62,tmpstr);
    NeoSetSolidFlag(1);
    NeoSetColor(GUI_PALETTE_PICK);
    NeoDrawRect(PANEL_LEFT+6,74,PANEL_LEFT+99,94);
    NeoSetColor(GUI_TEXTBOLD);
    NeoSetSolidFlag(0);
    tmpstr:='R';
    NeoDrawString(PANEL_LEFT+6,125,tmpstr);
    tmpstr:='G';
    NeoDrawString(PANEL_LEFT+6,140,tmpstr);
    tmpstr:='B';
    NeoDrawString(PANEL_LEFT+6,155,tmpstr);


    tmpstr:='ENTER:ok';
    DrawButton(PANEL_LEFT+26,174,GUI_BACK,tmpstr);
    tmpstr:='ESC:cancel';
    DrawButton(PANEL_LEFT+20,192,GUI_BACK,tmpstr);

end;


procedure EditColor(col:byte);
var done,refresh:boolean;
    pr,pg,pb:byte;
    crgb:array[0..2] of byte;
    cnum,xs,xc,y,sc:byte;
    x:word;

procedure DrawSlider;
begin
    NeoSetSolidFlag(1);
    x:=PANEL_LEFT+20;
    NeoSetColor(GUI_BACK);
    NeoDrawRect(x-3,y-1,x+78,y+5);
    NeoSetColor(GUI_BACKDARK);
    NeoDrawLine(x-2,y+2,x+78,y+2);
    for xc:=0 to 15 do begin
        if xc=xs then begin
            NeoSetColor(sc);
            NeoDrawEllipse(x-2,y,x+2,y+5);
        end else begin
            NeoSetColor(GUI_BLACK);
            NeoDrawLine(x,y+1,x,y+4);
        end;
        x:=x+5;
    end;
end;

function SliderClick:boolean;
var s:^byte;
begin
    result:=false;
    case mouse.areaId of
        21: s:=@crgb[0];
        22: s:=@crgb[1];
        23: s:=@crgb[2];
    end; 
    xs := (mouse.x - (PANEL_LEFT+18)) div 5;
    if xs<>(s^ shr 4) then begin
        s^:=xs shl 4;
        result:=true;
    end;
end;

procedure ShowColorSliders;
var scol:array [0..2] of byte = (GUI_BACKERROR,GUI_BACKOK,GUI_BOTTOM);
    sn:byte;
begin
    tmpstr:='#   ';
    y:=126;
    for sn:=0 to 2 do begin 
        sc:=scol[sn];
        xs:=crgb[sn] shr 4;
        tmpstr[sn+2]:=hexChars[xs];
        DrawSlider;
        inc(y,15);
    end;
    
    NeoSetSolidFlag(1);
    NeoSetColor(col);
    NeoDrawRect(PANEL_LEFT+6,94,PANEL_LEFT+99,114);
    
    NeoSetSolidFlag(0);
    NeoSetColor(GUI_BLACK);
    NeoDrawString(PANEL_LEFT+42,101,tmpstr);
    NeoSetColor(GUI_TEXT);
    NeoDrawString(PANEL_LEFT+41,100,tmpstr);

end;

begin
    cnum:=col;
    col:=currentPalette[col];
    currentTab := 6;
    NeoGetPalette(col);
    pr:=NeoMessage.params[1];
    pg:=NeoMessage.params[2];
    pb:=NeoMessage.params[3];
    NeoSetPalette(GUI_PALETTE_PICK,pr,pg,pb);
    crgb[0]:=pr;
    crgb[1]:=pg;
    crgb[2]:=pb;
    done := false;
    refresh := true;
    ShowColorPicker(cnum);

    repeat 
        NeoWaitForVblank;
        BlinkCursor;
        GetUserInput;

        if refresh then begin
            ShowColorSliders;
            NeoSetPalette(col,crgb[0],crgb[1],crgb[2]);
            refresh := false;
        end;

        {$i actions_coloredit.inc}

    until done;
    key.code := 0;
    currentTab := 4;
    StorePalettes;
    reload:=true;
end;

procedure ChooseColor(col:byte);
begin
    if col<>currentColor then begin 
        currentColor:=col;
        DrawPalette;
    end;
end;

procedure ChooseBackground(col:byte);
begin
    if col<>currentBackground then begin 
        currentBackground:=col;
        DrawPalette;
    end;
end;

procedure SwapColBack;
var t:byte;
begin
    t := currentBackground;
    currentBackground := currentColor;
    currentColor := t;
    DrawPalette;
end;

procedure ColorKeyPressed(col:byte);
begin
    //cursorMode := CUR_KEY;
    if key.ctrl then exit;
    if key.shift then
        ChooseBackground(col)
    else 
        ChooseColor(col);
    if (config.drawOnColorKey) and (currentTool=TOOL_DRAW) then DrawPixel(cursorX,cursorY,col);
end;

procedure SelectTool(tool:byte);
begin
    ClearCursor;
    currentTool := tool;
    DrawTools;
    MoveCursor(0,0);
end;

function Confirm(msg:string[56]):boolean;
begin
    msg:=Concat(msg,' - are you sure? [Y/N]');
    if config.noWarnMode then exit(true);
    BottomBar(GUI_CURSOR,GUI_BOTTOM,msg);
    WaitForNoInput;
    repeat 
        BlinkCursor;
        GetUserInput;
    until key.code<>0;
    result := key.code = KEY_Y;
    reload := true;
end;

procedure EditCurrent;
begin
    currentItemType:=currentTabType;
    currentItemId:=currentTabItem[currentTabType];
    currentItemWidth:=typeWidths[currentItemType];
    currentItemSize:=typeSizes[currentItemType];
    currentPalette:=typePalettes[currentItemType];
    LoadItem;
    InitCursor;
    scale:=config.zoom[currentItemType];
    SwitchTab(4);
    ClearUndos;
end;


procedure ImportElements;
var fpos:word;
    t:shortInt;
    cnt:byte;
    itype:shortInt;
    
begin
    cnt:=0;
    itype:=currentTab-1;
    GetFileName(tmpfile,'Select gfx file to import from:');
    if tmpfile<>'' then begin 
        if (NeoOpenFile(0,@tmpfile,OPEN_MODE_RO)) then begin
            if NeoReadFile(0,word(@importHeader),sizeOf(TGfxHeader))=4 then 
                if (importHeader.format = 1) and (importHeader.counts[itype]>0) then begin
                    fpos := $100;
                    for t:=0 to itype-1 do  
                        fpos := fpos + (typeSizes[t] * importHeader.counts[t]);
                    if NeoSeekPos(0,fpos) then begin
                        for t:=1 to importHeader.counts[itype] do begin
                            if AddItem(itype,gfxHeader.counts[itype]) then begin
                                fpos:=GetItemAddress(itype,gfxHeader.counts[itype]-1);
                                NeoReadFile(0,CANVAS_ADDRESS,typeSizes[itype]);
                                MoveR2G(CANVAS_ADDRESS,fpos,typeSizes[itype]);
                                inc(cnt);
                            end;
                        end;
                    end;
                    NeoStr(cnt,okMessage);
                    okMessage := Concat(okMessage,' items imported.');
                end;
            NeoCloseFile(0);
        end else begin
            Error('Error opening file.');
        end;
    end else Error('Operation Canceled.');        

    reload:=true;
end;

procedure LoadPalette;
var c:byte;
    rgb:array [0..2] of byte;
begin
    GetFileName(tmpfile,'Select palette file to load:');
    if tmpfile<>'' then begin 
        if (NeoOpenFile(0,@tmpfile,OPEN_MODE_RO)) then begin
            if NeoGetFileSize(0) = 3 * 16 then begin
                for c:=0 to 15 do begin
                    NeoReadFile(0, word(@rgb[0]), 3);
                    NeoSetPalette(c,rgb[0],rgb[1],rgb[2]);
                end;
                okMessage := 'Palette Loaded.';
                StorePalettes
            end;
            NeoCloseFile(0);
        end else begin
            Error('Error opening file.');
        end;
    end else Error('Operation Canceled.');        
    reload:=true;
end;

procedure SavePalette;
var c:byte;
    rgb:array [0..2] of byte;
begin
    GetFileName(tmpfile,'Select palette file to save:');
    if tmpfile<>'' then begin 
        if (NeoOpenFile(0,@tmpfile,OPEN_MODE_NEW)) then begin
                for c:=0 to 15 do begin
                    NeoMessage.params[0] := c;
                    NeoSendMessage(5, 38); // read pallette color
                    rgb[0]:=NeoMessage.params[1];
                    rgb[1]:=NeoMessage.params[2];
                    rgb[2]:=NeoMessage.params[3];
                    NeoWriteFile(0, word(@rgb[0]), 3);
                end;
                okMessage := 'File saved successfully.';
            NeoCloseFile(0);
        end else begin
            Error('Error opening file.');
        end;
    end else Error('Operation Canceled.');        
    reload:=true;
end;

procedure LoadNeo(fname:Tstring);
var size:cardinal;
    attr:byte;
begin
    if NeoStatFile(@fname,size,attr) then 
        if Confirm('Exit') then begin
            wordParams[0] := word(@fname);  
            asm 
                jmp $ffe8 
            end;
        end;
            
end;
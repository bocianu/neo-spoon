procedure Error(msg:TString);forward;
procedure OK(msg:TString);forward;
procedure ReadMouse;forward;
procedure DrawCursor(col:byte);forward;
procedure FixCursorView;forward;
procedure ToggleHelp;forward;
function Confirm(msg:string[56]):boolean;forward;

procedure setTmpKey(code:byte);
begin
    tmpKey.code:=code;
    tmpKey.shift:=false;
    tmpKey.ctrl:=false;
    tmpKey.alt:=false;
    tmpKey.meta:=false;
end;

function Sgn(v:integer):integer;
begin
    result := 0;
    if v < 0 then result := -1;
    if v > 0 then result := 1;
end;

procedure WaitForBlitterFree;
begin
    repeat until not NeoBlitterBusy;
end;

procedure ReadHeader;
begin
    WaitForBlitterFree;
    NeoBlitterCopy(MEM_GFX,word(@gfxHeader),$100);
end;

procedure StoreHeader;
begin
    WaitForBlitterFree;
    NeoBlitterCopy(GFX_HEADER_ADDRESS,MEM_GFX,$100);
end;

function MergeStrInt(s:Tstring;i:integer):TString;
begin
    NeoStr(i,result);
    result:=Concat(s,result);
end;


function IsGfxDataLoaded:boolean;
begin
    WaitForBlitterFree;
    NeoBlitterCopy(MEM_GFX,word(@importHeader),sizeOf(TGfxHeader));
    if importHeader.format <> 1 then exit(false);
    if importHeader.counts[0] or importHeader.counts[1] or importHeader.counts[2] = 0 then exit(false);
    result:=true;
end;

procedure NeoGetPalette(col:byte);
begin
    NeoMessage.params[0]:=col;
    NeoSendMessage(5,38);
end;

procedure StorePalettes;
var c:byte;
    buf:array [0..0] of byte absolute GFX_HEADER_ADDRESS+SizeOf(TGfxHeader);
    off:word;
    shift:byte;
begin
    off:=0;
    for c:=0 to 15 do begin
        NeoMessage.params[0]:=c;
        NeoSendMessage(5,38);
        buf[off]   := NeoMessage.params[1] shr 4;
        buf[off+1] := NeoMessage.params[2] shr 4;
        buf[off+2] := NeoMessage.params[3] shr 4;
        NeoMessage.params[0]:=c shl 4;
        NeoSendMessage(5,38);
        buf[off]   := (NeoMessage.params[1] and $f0) or buf[off];
        buf[off+1] := (NeoMessage.params[2] and $f0) or buf[off+1];
        buf[off+2] := (NeoMessage.params[3] and $f0) or buf[off+2];
        inc(off,3);
    end;
    StoreHeader;
end;

procedure RestorePalettes;
var c:byte;
    buf:array [0..0] of byte absolute GFX_HEADER_ADDRESS + SizeOf(TGfxHeader);
    off:word;
begin
    off:=0;
    for c:=0 to 15 do begin
        NeoMessage.params[1] := buf[off] shl 4;
        NeoMessage.params[2] := buf[off+1] shl 4;
        NeoMessage.params[3] := buf[off+2] shl 4;
        NeoMessage.params[0] := c;
        NeoSendMessage(5,32);
        NeoMessage.params[1] := buf[off] and $f0;
        NeoMessage.params[2] := buf[off+1] and $f0;
        NeoMessage.params[3] := buf[off+2] and $f0;
        NeoMessage.params[0] := c shl 4;
        NeoSendMessage(5,32);
        Inc(off,3);
    end;
end;

function IsPaletteStored:boolean;
var c:byte;
    buf:array [0..0] of byte absolute GFX_HEADER_ADDRESS + SizeOf(TGfxHeader);
begin
    result := false;
    for c := 0 to 47 do begin
        if buf[c] <> 0 then result := true;
    end;    
end;

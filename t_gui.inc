var
    charOff: array [0..6] of byte = 
    (
        %00000000,
        %11111100,
        %10000100, 
        %10000100,
        %10000100,
        %10000100,
        %11111100
    );
    charOn: array [0..6] of byte = 
    (
        %00000000,
        %11111100,
        %10000100, 
        %10110100,
        %10110100,
        %10000100,
        %11111100
    );

procedure RefreshGuiPalette;
begin
    //NeoResetPalette;
    NeoSetPalette(GUI_BLACK,0,0,0);            // black
    NeoSetPalette(GUI_TEXT,200,200,200);      // light grey - default text
    NeoSetPalette(GUI_BOTTOM,0,$30,$80);          // dark blue - bottom bar 
    NeoSetPalette(GUI_BACK,$50,$50,$50);         // dark grey - default back
    NeoSetPalette(GUI_CURSOR,250,100,120);      // blinking cursor
    NeoSetPalette(GUI_BACKDARK,$30,$30,$30);         // darker grey - inactive tab
    NeoSetPalette(GUI_BACKOK,0,$80,$10);         // green  - OK bar 
    NeoSetPalette(GUI_BACKERROR,$80,$10,0);          // red  - ERROR bar 
    NeoSetPalette(GUI_TEXTBOLD,240,220,100);      // yello  - light text
    NeoSetPalette(GUI_TOOL,40,120,220);      // selected tool bg
end;

procedure WaitForNoInput;
begin
    repeat 
        ReadKeyboard(key);
        key.code := key.code or NeoGetJoy(0);
    until key.code = 0;
end;

procedure BlinkCursor;
begin
    Inc(frame);
    NeoSetPalette(GUI_CURSOR,frame shl 4,frame shl 2,frame shl 1);
end;

function Min(a,b:word):word;
begin
    if a < b then result := a else result := b;
end;




procedure ShowCursorInfo;
begin
    NeoSetColor(GUI_BACK);
    NeoSetSolidFlag(1);
    NeoDrawRect(200,228,320,238);
    NeoSetColor(GUI_BACKDARK);
    NeoSetSolidFlag(0);
    tmpstr := MergeStrInt('X:',cursorX);
    NeoDrawString(200,228,tmpstr);
    tmpstr := MergeStrInt('Y:',cursorY);
    NeoDrawString(240,228,tmpstr);
    tmpstr := MergeStrInt('No:',currentTile);
    NeoDrawString(280,228,tmpstr);
end;

procedure DrawCursor(col:byte);
var px,py:word;
    ex,ey:word;
begin
    if cursorMode and CUR_KEY <> 0 then begin
        px := (cursorX * 16) - tileMapXoff;
        py := TMAP_TOP + (cursorY * 16)  - tileMapYoff;
        ex := px+15;
        ey := py+15;
        NeoSetColor(col);
        NeoSetSolidFlag(0);
        NeoDrawRect(px,py,ex,ey);
    end;
    ShowCursorInfo;
end;

procedure InitCursor;
begin
    cursorX:=0;
    cursorY:=0;
end;

procedure MoveCursor(dx,dy:byte);
begin
    cursorMode := cursorMode or CUR_KEY;
    if dx = -1 then if cursorX > 0 then Dec(cursorX);
    if dx = 1 then if cursorX < tileMapHeader.width - 1 then Inc(cursorX);
    if dy = -1 then if cursorY > 0 then Dec(cursorY);
    if dy = 1 then if cursorY < tileMapHeader.height - 1 then Inc(cursorY);
    FixCursorView;
    reloadboard := true;
end;

procedure GetUserInput;
begin
    if NeoIsMousePresent then ReadMouse;
    ReadKeyboard(key);

    if key.code = 0 then begin
        joyDelay := 0;
        joyCount := 0;
    end;
    
    if joyDelay = 0 then begin 
        if key.code <> 0 then begin 
            joyDelay := INPUT_DELAY;
            if joyCount > 0 then begin
                joyDelay := JOY_REPEAT;
            end else Inc(joyCount);
        end;
    end else begin  
        Dec(joyDelay);
        key.code := 0;
    end;

end;

procedure GetFileName(var s:string;hdr:TString);
var tstr:TString;
begin
    while keypressed do readkey;
    StorePalettes;
    NeoResetPalette;
    NeoSetTextColor(15,0);
    ClrScr;
    NeoShowDir;
    tstr:='';
    Writeln;
    Writeln(hdr);
    Writeln('Enter filename: ');
    Readln(tstr);
    move(@tstr,@s,Length(tstr)+1);
    RestorePalettes;
    RefreshGuiPalette;
    reload:=true;
end;

procedure DrawFrame(x:word;y,w,h,fc,bc:byte);
begin   
    if w>0 then dec(w);
    if h>0 then dec(h);
    NeoSetSolidFlag(1);
    NeoSetColor(bc);
    NeoDrawRect(x,y,x+w,y+h);
    NeoSetSolidFlag(0);
    NeoSetColor(fc);
    NeoDrawRect(x,y,x+w,y+h);
end;

procedure BottomBar(fc,bc:byte;var s:string);
begin
    NeoSetColor(GUI_BACKDARK);
    NeoSetSolidFlag(1);
    NeoDrawLine(0,228,319,228);
    NeoSetColor(bc);
    NeoDrawRect(0,229,319,239);
    NeoSetColor(GUI_BLACK);
    NeoSetSolidFlag(0);
    NeoDrawString(3,232,s);
    NeoSetColor(fc);
    NeoDrawString(2,231,s);
end;

procedure DrawButton(x:word;y:byte;fc:byte;var s:string);
var w:byte;
begin
    w:=(Length(s)*6)+6;
    DrawFrame(x,y,w,BUT_H,fc,GUI_BOTTOM);
    NeoSetColor(GUI_BLACK);
    NeoSetSolidFlag(0);
    NeoDrawString(x+4,y+4,s);
    NeoSetColor(GUI_TEXTBOLD);
    NeoDrawString(x+3,y+3,s);
end;

procedure ShowMainScreen;
var
    t:byte;
    x:word;
    col,bcol:byte;
begin
    NeoSetColor(GUI_BLACK);
    NeoSetSolidFlag(1);
    NeoDrawRect(0,0,319,10);            
    x:=0;
    for t:=0 to TAB_COUNT-1 do begin
        if (t=currentTab) or (t=helpScreen) then begin
            bcol:=GUI_BACK;
            col:=GUI_TEXT;
        end else begin
            col:=GUI_BACK;
            bcol:=GUI_BACKDARK;
        end;
        NeoSetColor(bcol);
        NeoSetSolidFlag(1);
        NeoDrawRect(x,0,x+TAB_WIDTH-2,10);            
        NeoSetColor(GUI_BLACK);
        NeoSetSolidFlag(0);
        tmpstr := tabnames[t];
        NeoDrawString(x+3,2,tmpstr);
        NeoSetColor(col);
        NeoDrawString(x+2,1,tmpstr);
        x:=x+TAB_WIDTH;
    end;
    if helpScreen=$ff then begin
        col:=GUI_BACK;
    end else begin
        col:=GUI_TEXTBOLD;
    end;
    NeoSetColor(col);
    tmpstr:='?';
    NeoDrawString(314,2,tmpstr);
end;

procedure SwitchTab(tnum:byte);
begin
    if tnum=$ff then tnum:=TAB_COUNT-1;
    if tnum=TAB_COUNT then tnum:=0;
    if helpScreen<>$ff then ToggleHelp;
    if tnum <> currentTab then begin
        if currentTab=1 then saveMapTmp;
        if currentTab=3 then ResizeIfNeeded;
        if tnum = 1 then FixCursorView;
        currentTab := tnum;
        reload := true;
        inputDelay := INPUT_DELAY;
    end;
end;

procedure ShowConfigTab;
var 
    oy:byte;

procedure DrawOptBool(state:boolean;hotkey:byte;s:string[53]);
begin
    NeoSetSolidFlag(0);
    NeoSetColor(GUI_TEXT);
    if state then tmpstr:=#193 else tmpstr:=#192;
    tmpstr:=Concat(tmpstr,s);
    NeoDrawString(16,oy,tmpstr);            
    NeoSetColor(GUI_TEXTBOLD);
    tmpstr:=s[hotkey];
    NeoDrawString(16+(hotkey*6),oy,tmpstr);            
    Inc(oy,16);
end;

procedure DrawOptByte(val:byte;hotkey:byte;s:string[53]);
begin
    NeoSetSolidFlag(0);
    NeoSetColor(GUI_TEXT);
    tmpstr:=MergeStrInt(s,val);
    NeoDrawString(16,oy,tmpstr);            
    NeoSetColor(GUI_TEXTBOLD);
    tmpstr:=s[hotkey];
    NeoDrawString(10+(hotkey*6),oy,tmpstr);            
    Inc(oy,16);
end;

begin
    oy:=30;
    DrawOptBool(config.noWarnMode,2,' Disable Confirmations (YOLO mode)');
    DrawOptBool(config.showGrid,7,' Show Grid');
    DrawOptByte(config.mapWidth,5,'Map Width: ');
    DrawOptByte(config.mapHeight,5,'Map Height: ');

    //DrawOptBool(config.wrapImage,2,' Wrap Image');
    //DrawOptBool(config.drawOnColorKey,24,' Draw when you press a Color key');
   
    tmpstr := '^R:Restore Defaults';
    BottomBar(GUI_TEXT,GUI_BOTTOM,tmpstr);
end;

procedure ShowFileTab;
var p:word;
begin
    NeoSetColor(GUI_TEXTBOLD);
    NeoSetSolidFlag(0);
    tmpstr:=Concat('Map file: ', mapfile);
    NeoDrawString(3,30,tmpstr);            
    NeoSetColor(GUI_TEXT);
    tmpstr:=MergeStrInt('Map width:  ', tileMapHeader.width);
    NeoDrawString(3,50,tmpstr);
    tmpstr:=MergeStrInt('Map height: ', tileMapHeader.height);
    NeoDrawString(3,60,tmpstr);
    tmpstr:=MergeStrInt('Map size: ', tileMapSize);
    tmpstr:=concat(tmpstr, ' bytes (');
    p:=(tileMapSize*100) div TILEMAP_MAXSIZE;
    tmpstr := MergeStrInt(tmpstr, p);
    tmpstr := Concat(tmpstr, '%)');
    NeoDrawString(3,90,tmpstr);
    p:=(tileMapSize*311) div TILEMAP_MAXSIZE;
    NeoSetColor(GUI_BLACK);
    NeoSetSolidFlag(1);
    NeoDrawRect(2,100,317,110);
    if p>0 then begin
        NeoSetColor(GUI_TEXT);
        NeoDrawRect(4,102,p+4,108);
    end;



    NeoSetColor(GUI_BACKDARK);
    NeoSetSolidFlag(0);
    tmpstr:='T-Spoon - Neo6502 tile map editor';
    NeoDrawString(3,218,tmpstr);
    tmpstr:=Concat('version ', VER);
    NeoDrawString(243,218,tmpstr);
    
    tmpstr := '^N:New  ^O:Open  ^S:Save  ^D:Save as  TAB:Switch';
    BottomBar(GUI_TEXT,GUI_BOTTOM,tmpstr);

end;

procedure DrawTile(x,y:word;t:byte);
begin
    NeoSetSolidFlag(1);
    if t<$f0 then NeoSetColor(0) else NeoSetColor(t and $f);
    NeoDrawRect(x,y,x+15,y+15);
    if t<$f0 then NeoDrawImage(x,y,t);
end;  

procedure DrawTileInfo;
begin
    NeoSetColor(GUI_BACK);
    NeoSetSolidFlag(1);
    NeoDrawRect(312-104,218,312,226);
    NeoSetColor(GUI_BACKDARK);
    NeoSetSolidFlag(0);
    tmpstr := MergeStrInt('element No:',currentTile);
    NeoDrawString(312-(Length(tmpstr)*6),218,tmpstr);
end;

procedure ShowCurrentTiles;
begin
    NeoSetColor(GUI_BLACK);
    NeoSetSolidFlag(1);
    NeoDrawRect(1,223,16,238);
    NeoDrawRect(18,223,33,238);
    DrawTile(1,223,currentTile);
    DrawTile(18,223,currentBackground);
end;


procedure ShowMapTab;
var x:word;
    y:byte;    
begin
    NeoSetColor(0);
    NeoSetSolidFlag(1);
    NeoDrawRect(0,TMAP_TOP-1,320,TMAP_TOP+208);
    NeoSelectTileMap(TILEMAP_HEADER,tileMapXoff,tileMapYoff);
    NeoDrawTileMap(0,TMAP_TOP,320,TMAP_TOP+208);
    if config.showGrid then begin
        x:=15-(tileMapXoff and 15);
        y:=15-(tileMapYoff and 15) + TMAP_TOP;
        NeoSetColor(GUI_BACKDARK);
        while x<319 do begin
            NeoDrawLine(x,TMAP_TOP,x,TMAP_TOP+208);
            x:=x+16;
        end; 
        while y<TMAP_TOP+207 do begin
            NeoDrawLine(0,y,319,y);
            y:=y+16;
        end; 
    end;
    DrawCursor(GUI_CURSOR);
    ShowCurrentTiles;
end;
  

procedure DrawElements;
var c,i,col,w:byte;
    x,y:word;
begin
    w:=16;
    for c:=1 to gfxHeader.counts[0] do begin
        i:=c-1;
        x:=(i mod 16)*20+2;
        y:=(i div 16)*20+16;
        if i=currentTile then col:=GUI_CURSOR else col:=0;
        NeoSetColor(col);
        NeoSetSolidFlag(0);
        NeoDrawRect(x-1,y-1,x+w,y+w);
        DrawTile(x,y,i);
        if i=currentBackground then begin
            NeoSetColor(GUI_CURSOR);
            tmpstr := 'B';
            NeoDrawString(x+5,y+4,tmpstr);
        end;
    end;
end;

procedure DrawSolids;
var c,i,col,w:byte;
    x,y:word;
begin
    w:=16;
    y:=180;
    for c:=0 to 15 do begin
        i:=$f0 or c;
        x:=c*20+2;
        if i=currentTile then col:=GUI_CURSOR else col:=0;
        DrawTile(x,y,i);
        NeoSetColor(col);
        NeoSetSolidFlag(0);
        NeoDrawRect(x-1,y-1,x+w,y+w);

        if i=currentBackground then begin
            NeoSetColor(GUI_CURSOR);
            tmpstr := 'B';
            NeoDrawString(x+5,y+4,tmpstr);
        end;
    end;
end;


procedure ShowTilesTab;

begin

    NeoStr(gfxHeader.counts[0],tmpstr);
    tmpstr := concat(tmpstr,' elements - size: ');
    tmpstr := MergeStrInt(tmpstr,gfxHeader.counts[0]*128);
    NeoSetColor(GUI_BACKDARK);
    NeoSetSolidFlag(0);
    NeoDrawString(3,218,tmpstr);

    if gfxHeader.counts[0]=0 then begin
    tmpstr := 'No tiles in memory.';
    NeoDrawString(3,15,tmpstr);
    end;

    tmpstr:='Load Tileset';
    DrawButton(3,202,GUI_BACKDARK,tmpstr);

    tmpstr:='Load Palette';
    DrawButton(84,202,GUI_BACKDARK,tmpstr);

    if spoonExists then begin
        tmpstr:='F10 - Edit Tiles';
        DrawButton(214,202,GUI_BACKDARK,tmpstr);
    end;

    tmpstr := '^L:Load Tileset  ^P:Load Palette';
    BottomBar(GUI_TEXT,GUI_BOTTOM,tmpstr);
    
    DrawElements;
    DrawSolids;
    DrawTileInfo;

end;

procedure ShowTool(x:word;y,tool:byte;s:string[8]);
var w:byte;
    bcol,tcol:byte;
begin
    bcol := GUI_BOTTOM;
    tcol := GUI_TEXT;
    if (currentTool=tool) then begin
        bcol:=GUI_TOOL;
        tcol := GUI_TEXTBOLD;
    end;
    w:=(Length(s)*6)+6;
    DrawFrame(x,y,w,BUT_H,GUI_BACK,bcol);
    NeoSetColor(GUI_BLACK);
    NeoSetSolidFlag(0);
    NeoDrawString(x+4,y+4,s);
    NeoSetColor(tcol);
    NeoDrawString(x+3,y+3,s);
end;

procedure Error(msg:TString);
begin
    BottomBar(GUI_TEXT,GUI_BACKERROR,msg);
    WaitForNoInput;
    repeat GetUserInput until key.code<>0;
    key.code:=0;
end;

procedure OK(msg:TString);
begin
    BottomBar(GUI_TEXT,GUI_BACKOK,msg);
    delayedReload := OK_MESSAGE_DELAY;
end;


function Confirm(msg:string[56]):boolean;
begin
    msg:=Concat(msg,' - are you sure? [Y/N]');
    if config.noWarnMode then exit(true);
    BottomBar(GUI_CURSOR,GUI_BOTTOM,msg);
    WaitForNoInput;
    repeat 
        BlinkCursor;
        GetUserInput;
    until key.code<>0;
    result := key.code = KEY_Y;
    reload := true;
end;

procedure LoadPalette;
var c:byte;
    rgb:array [0..2] of byte;
begin
    GetFileName(tmpfile,'Select palette file to load:');
    if tmpfile<>'' then begin 
        if (NeoOpenFile(0,@tmpfile,OPEN_MODE_RO)) then begin
            if NeoGetFileSize(0) = 3 * 16 then begin
                for c:=0 to 15 do begin
                    NeoReadFile(0, word(@rgb[0]), 3);
                    NeoSetPalette(c,rgb[0],rgb[1],rgb[2]);
                end;
                okMessage := 'Palette Loaded.';
                StorePalettes;
            end;
            NeoCloseFile(0);
        end else begin
            Error('Error opening file.');
        end;
    end else Error('Operation Canceled.');        
    reload:=true;
end;

procedure LoadNeo(fname:Tstring);
begin
    if Confirm('Exit') then begin
        wordParams[0] := word(@fname);  
        asm 
            jmp $ffe8 
        end;
    end;
end;

function FileExists(fname:Tstring):boolean;
var size:cardinal;
    attr:byte;
begin
    result := NeoStatFile(@fname,size,attr);
end;


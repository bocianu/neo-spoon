const 
    MMAP_SIZE = 32;
    
procedure TilesClick;
var clickId:byte;
begin
    clickId := (mouse.x div typeBoxsize[currentTabType]) + (((mouse.y - 15) div typeBoxsize[currentTabType]) * typeCols[currentTabType]);
    clickId := clickId + (itemPage*32);
    if (clickId<=itemLast) and (gfxHeader.counts[currentTabType]>0) then begin
        currentTabItem[currentTabType]:=clickId;
        if mouse.dblclick > 0 then EditCurrent;
        mouseDelay:=30;
        reloadboard:=true;
    end else begin
        if mouse.dblclick > 0 then key.code := KEY_EQUAL;
    end;
end;

procedure PaletteClick;
var col:byte;
begin
    col := mouse.x div 20;
    if mouse.dblclick > 0 then begin
        ChooseColor(col);
        EditColor(col);
        exit;
    end;
    if mouse.but and 2 <> 0 then ChooseBackground(col)
    else ChooseColor(col);
end;

var canvasPixelX,canvasPixelY:byte;

function IsCanvasClicked:boolean;
begin
    canvasPixelX := (mouse.x - BOARD_X0) div (scale+1);
    canvasPixelY := (mouse.y - BOARD_Y0) div (scale+1);
    result := (canvasPixelX<currentItemWidth) and (canvasPixelY<currentItemWidth);
end;

procedure CanvasClick;
begin
    if IsCanvasClicked then begin
        case currentTool of
            TOOL_FILL: Fill(canvasPixelX,canvasPixelY);
        end;
        ClearCursor;
        cursorX:=canvasPixelX;
        cursorY:=canvasPixelY;
    end;
end;

procedure CanvasMiddleClick;
begin
    if IsCanvasClicked then begin
        ChooseColor(GetPixelColor(canvasPixelX,canvasPixelY));
    end;
end;

procedure CanvasPressed;
var c:byte;
begin
    if IsCanvasClicked then
        case currentTool of
            TOOL_DRAW: begin
                c:=currentColor;
                if mouse.but and 2 <> 0 then c:=currentBackground;
                //if GetPixelColor(canvasPixelX,canvasPixelY)<>c then begin
                    ClearCursor;
                    cursorX:=canvasPixelX;
                    cursorY:=canvasPixelY;
                    DrawPixel(cursorX,cursorY,c);
                //end;
            end;
        end;
end;
procedure Error(msg:TString);forward;
procedure OK(msg:TString);forward;
procedure ReadMouse;forward;
procedure UpdateItem;forward;
procedure ToggleHelp;forward;
procedure StoreToUndoOrigin;forward;
procedure SaveUndoCmd(cmd:byte);forward;
procedure SaveUndoCanvas;forward;
procedure ClearUndos;forward;

function GetDataSize:word;
var t:byte;
begin
    result := $100;
    for t := 0 to 2 do result := result + (typeSizes[t] * gfxHeader.counts[t]);
end;

function Sgn(v:integer):integer;
begin
    result := 0;
    if v < 0 then result := -1;
    if v > 0 then result := 1;
end;

procedure WaitForBlitterFree;
begin
    repeat until not NeoBlitterBusy;
end;

procedure MoveG2G(src,dest,len:word);
begin
    WaitForBlitterFree;
    NeoBlitterCopy(MEM_GFX+src,MEM_GFX+dest,len);
end;

procedure MoveR2G(src,dest,len:word);
begin
    WaitForBlitterFree;
    NeoBlitterCopy(src,MEM_GFX+dest,len);
end;

procedure MoveG2R(src,dest,len:word);
begin
    WaitForBlitterFree;
    NeoBlitterCopy(MEM_GFX+src,dest,len);
end;

procedure FillGByte(dest,len:word;b:byte);
const BUF_SIZE = 512;
var dlen:word;
begin
    fillbyte(@canvas,512,b);
    while len>0 do begin
        dlen:=BUF_SIZE;
        if len<dlen then dlen:=len;
        MoveR2G(CANVAS_ADDRESS,MEM_GFX+dest,dlen);
        dec(len,dlen);
        Inc(dest,dlen);
    end;    
end;

procedure StoreHeader;
begin
    WaitForBlitterFree;
    NeoBlitterCopy(GFX_HEADER_ADDRESS,MEM_GFX,$100);
end;

procedure ReadHeader;
begin
    WaitForBlitterFree;
    NeoBlitterCopy(MEM_GFX,GFX_HEADER_ADDRESS,$100);
end;

function IsGfxDataLoaded:boolean;
begin
    WaitForBlitterFree;
    NeoBlitterCopy(MEM_GFX,word(@importHeader),sizeOf(TGfxHeader));
    if importHeader.format <> 1 then exit(false);
    if importHeader.counts[0] or importHeader.counts[1] or importHeader.counts[2] = 0 then exit(false);
    result:=true;
end;

function MergeStrInt(s:Tstring;i:integer):TString;
begin
    NeoStr(i,result);
    result:=Concat(s,result);
end;

procedure NeoGetPalette(col:byte);
begin
    NeoMessage.params[0]:=col;
    NeoSendMessage(5,38);
end;

procedure SwapPalettes;
var i,r,g,b:byte;
begin
    for i := 1 to 15 do begin
        NeoGetPalette(i);
        r := NeoMessage.params[1];
        g := NeoMessage.params[2];
        b := NeoMessage.params[3];
        NeoGetPalette(i shl 4);
        NeoMessage.params[0] := i;
        NeoSendMessage(5,32); // set palette
        NeoSetPalette(i shl 4,r,g,b);
    end;
    isPaletteSwapped := not isPaletteSwapped;
end;

procedure StorePalettes;
var c:byte;
    buf:array [0..0] of byte absolute GFX_HEADER_ADDRESS+SizeOf(TGfxHeader);
    off:word;
    shift:byte;
    swap:boolean;
begin
    swap := isPaletteSwapped;
    if swap then SwapPalettes;
    off:=0;
    for c:=0 to 15 do begin
        NeoMessage.params[0]:=c;
        NeoSendMessage(5,38);
        buf[off]   := NeoMessage.params[1] shr 4;
        buf[off+1] := NeoMessage.params[2] shr 4;
        buf[off+2] := NeoMessage.params[3] shr 4;
        NeoMessage.params[0]:=c shl 4;
        NeoSendMessage(5,38);
        buf[off]   := (NeoMessage.params[1] and $f0) or buf[off];
        buf[off+1] := (NeoMessage.params[2] and $f0) or buf[off+1];
        buf[off+2] := (NeoMessage.params[3] and $f0) or buf[off+2];
        inc(off,3);
    end;
    StoreHeader;
    if swap then SwapPalettes;
end;

procedure RestorePalettes;
var c:byte;
    buf:array [0..0] of byte absolute GFX_HEADER_ADDRESS + SizeOf(TGfxHeader);
    off:word;
    swap:boolean;
begin
    swap := isPaletteSwapped;
    if swap then SwapPalettes;
    off:=0;
    for c:=0 to 15 do begin
        NeoMessage.params[1] := buf[off] shl 4;
        NeoMessage.params[2] := buf[off+1] shl 4;
        NeoMessage.params[3] := buf[off+2] shl 4;
        NeoMessage.params[0] := c;
        NeoSendMessage(5,32);
        NeoMessage.params[1] := buf[off] and $f0;
        NeoMessage.params[2] := buf[off+1] and $f0;
        NeoMessage.params[3] := buf[off+2] and $f0;
        NeoMessage.params[0] := c shl 4;
        NeoSendMessage(5,32);
        Inc(off,3);
    end;
    if swap then SwapPalettes;
end;

function IsPaletteStored:boolean;
var c:byte;
    buf:array [0..0] of byte absolute GFX_HEADER_ADDRESS + SizeOf(TGfxHeader);
begin
    result := false;
    for c := 0 to 47 do begin
        if buf[c] <> 0 then result := true;
    end;    
end;


procedure setTmpKey(code:byte);
begin
    tmpKey.code:=code;
    tmpKey.shift:=false;
    tmpKey.ctrl:=false;
    tmpKey.alt:=false;
    tmpKey.meta:=false;
end;

procedure ExpandRLE(src: word; dest: word);
var value, count:byte;
begin
    value := peek(src);
    while value<>0 do begin
        Inc(src);
        count := (value shr 1) + 1;
        if Odd(value) then begin // just repeat data
            Move(pointer(src),pointer(dest),count);
            Inc(src,count);
        end else begin  // expand
            FillChar(pointer(dest),count,peek(src));
            Inc(src);
        end;
        Inc(dest,count);
        value := peek(src);
    end;
end; 

function PackRLE(src: word; size:word; dest: word; limit:word):word;
var x,oldx,sizeaddr:word;
    a,count:byte;

procedure WriteDest(b:byte);
begin
    if result<=limit then begin
        poke(dest,b);
        inc(dest);
        inc(result);
    end;
end;

procedure saveRLE;inline;
begin
    WriteDest((count-1) shl 1);
    WriteDest(a);
end;

procedure saveCopy;inline;
begin
    x := oldx;
    sizeaddr:=dest;
    WriteDest(0); // temporary size
    count:=0;
    while (x < size) and (count <= 127) do begin
        a:=peek(src+x);
        WriteDest(a);
        if ((x < size-2) and (a=peek(src+x+1)) and (a=peek(src+x+2))) then begin
            dec(result);
            Dec(dest);
            break;
        end; 
        inc(count);
        inc(x);
    end;
    dec(count);
    a:=(count shl 1) + 1;
    poke(sizeaddr,a);
end;

begin
    x:=0;
    oldx:=0;
    result:=0;
    while(x < size) do begin
        oldx := x;
        a := peek(src + x);
        count := 1;
        Inc(x);
        while ((x < size) and (a = peek(src + x))) do begin
            Inc(x);
            Inc(count);
            if count = 127 then break;
        end;
        if count > 2 then saveRLE
        else saveCopy;
        if result > limit then break;
    end;
    WriteDest(0);
    if result > limit then result:=0;
end; 

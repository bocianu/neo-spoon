

function ScanForArea(x,y:word):byte;
var off:word;
begin
    off := MMAP_ADDRESS;
    repeat
        mArea:=pointer(off);
        if (mArea.tab = TAB_ALL) or ((tabs[currentTab] and mArea.tab) <> 0) then 
            if (x>=mArea.x) and (x<=(mArea.x+mArea.w)) then
                if (y>=mArea.y) and (y<=(mArea.y+mArea.h)) then
                    exit(mArea.areaId);
        off:=off+SizeOf(TMouseArea);
    until mArea.tab = 0;
    result:=$ff;
end;

procedure MouseMoved;
begin
    cursorMode := cursorMode or CUR_MOUSE;
    NeoSetCursorXY(mouse.x,mouse.y);
end;


procedure ReadMouse;
var wheeldelta:shortInt;
begin
    if mouse.dblclick>0 then mouse.dblclick:=mouse.dblclick-1;

    NeoReadMouse;
    mouse.but := neoMouseButtons;
    mouse.wheel := neoMouseWheel;
    mouse.x:=neoMouseX;
    mouse.y:=neoMouseY;

    // wheel processing
    wheeldelta := mouse.wheel - mWheelPrev;
    mWheelPrev := mouse.wheel;
    mouse.wheel := Sgn(wheeldelta);

    // button processing
    mouse.butchange := mouse.but xor mButPrev;
    mButPrev := mouse.but;

    // position processing
    if (prevMouseX<>mouse.x) or (prevMouseY<>mouse.y) then begin    
        MouseMoved;
        mouse.idle := 0;
    end else Inc(mouse.idle);
    if mouse.butchange <> 0 then mouse.idle := 0;


    prevMouseX:=mouse.x;
    prevMouseY:=mouse.y;

    mouse.LDown := mouse.butchange and mouse.but and 1 <> 0;
    mouse.RDown := mouse.butchange and mouse.but and 2 <> 0;
    mouse.MDown := mouse.butchange and mouse.but and 4 <> 0;
    mouse.LUp := (mouse.butchange and 1 <> 0) and (mouse.but and 1 = 0);
    mouse.RUp := (mouse.butchange and 2 <> 0) and (mouse.but and 2 = 0);
    mouse.MUp := (mouse.butchange and 4 <> 0) and (mouse.but and 4 = 0);

    if mouse.LUp then begin  
        mouse.dblclick := DBL_CLICK_LEN;
    end;

    mouse.areaId := $ff;

    if (mouse.butchange or mouse.but or mouse.wheel) = 0 then exit;
    cursorMode := cursorMode or CUR_MOUSE;
    mouse.areaId := ScanForArea(mouse.x,mouse.y);

    //gotoxy(1,1); Writeln(prevMouseX,' ',prevMouseY,' ');


end;


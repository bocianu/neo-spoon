    
if mouse.LDown then 
    case mouse.areaId of
        // tabs
        1:key.code := KEY_F1;
        2:key.code := KEY_F2;
        3:key.code := KEY_F3;
        4:key.code := KEY_F4;
        // help
        8: begin key.shift:=true; key.code := KEY_SLASH; end; 
    end;

case key.code of

    KEY_TAB: if key.shift then SwitchTab(currentTab - 1)
        else SwitchTab(currentTab + 1);
    KEY_F1: SwitchTab(0);
    KEY_F2: SwitchTab(1);
    KEY_F3: SwitchTab(2);
    KEY_F4: SwitchTab(3);

    KEY_S: if key.ctrl then begin // ctrl S
        if mapfile='' then GetFileName(mapfile,'Saving file as:');
        if mapfile<>'' then begin
            if NeoSave(mapfile,TILEMAP_HEADER,tileMapSize+3) then begin
                okMessage := 'File saved successfully.';
            end else Error('Error saving file.');                    
        end else Error('Operation Canceled.');                    
        reload := true;
    end;

    KEY_O: if key.ctrl then begin // ctrl O
        GetFileName(tmpfile,'Loading Map file:');
        if tmpfile<>'' then begin 
            if LoadMapFile(tmpfile) then begin
                mapfile := tmpfile;
            end else begin
                Error('Error opening file.');
            end;
        end else Error('Operation Canceled.');        
        reload := true;
    end;

    KEY_D: if key.ctrl then begin // ctrl D
        GetFileName(tmpfile,'Saving file as:');
        if tmpfile<>'' then begin 
            if NeoSave(tmpfile,TILEMAP_HEADER,tileMapSize+3) then begin
                mapfile := tmpfile;
                okMessage := 'File saved successfully.';
            end else Error('Error saving file.');    
        end else Error('Operation Canceled.');        
        reload := true;
    end;

    KEY_N: if key.ctrl then begin // ctrl N
        if Confirm('Clear') then NewMap;
    end;

    KEY_Q: if key.ctrl then begin // ctrl Q
        if Confirm('Exit') then quit:=true;
    end;

    KEY_SLASH: if key.shift then ToggleHelp;

    KEY_ESC: if helpScreen<>$ff then ToggleHelp;

    KEY_F10: if spoonExists then begin 
        saveMapTmp; 
        LoadNeo(SPOON_NEO); 
    end;

end;
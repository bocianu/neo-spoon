procedure ToggleHelp;
begin
    if helpScreen<>$ff then begin // close 
        currentTab:=helpScreen;
        helpScreen:=$ff;
    end else begin // open
        helpScreen:=currentTab;
        currentTab:=$ff;
    end;
    reload:=true;
end;

procedure ShowHelp(page:byte);
var helpline:byte;
helpFile: pchar = 
'File shortcuts are available on all tabs:'#0#0+
'ctrl+N : create a new asset container'#0+
'ctrl+O : open the asset container from the file'#0+
'ctrl+S : save the current container to a file'#0+
'ctrl+D : save the current container as a new file'#0#0+
'To switch between tabs, you can use the TAB'#0+
'or shift+TAB key, or the F1-F6 keys.'#0+
'The "?" key allows you to get contextual'#0+
'help about the current tab contents/shortcuts.'#0#$ff;

helpList: pchar = 
'The tab contains icons of items you can edit.'#0+
'To select an item for editing, click on it,'#0+
'or select it with the cursor keys and press ENTER.'#0#0+
'Double-click in an empty area to add a new item.'#0#0+
'Keyboard shortcuts:'#0+
'DELETE : delete the selected item'#0+
'INSERT : insert a new element at cursor'#0+
'+ : insert a new element at the end of the list'#0+
'ctrl+C : copy the element to the clipboard'#0+
'ctrl+V : insert an element from the clipboard'#0+
'I : import items from another file'#0+
'L : load palette'#0+
'S : save palette'#0#$ff;

helpEditor: pchar = 
'Edit window keyboard shortcuts:'#0#0+
'+/- : resize the edit box'#0+
'0-9,A-F : choose a drawing color (shift - back)'#0+
'SPACE/DELETE : place/delete a pixel'#0+
'</> : switch to previous/next item'#0+
'PgUp/PgDn : switch drawing color (shift - back)'#0+
'~/` : change brush size'#0+
'ctrl+Z/Y : Undo/Redo'#0+
'I,P,S : tool shortcuts'#0+
'G : show grid'#0+
'X : toggle color/back'#0+
'BACKSPACE : clear the canvas with the back color'#0+
'ctrl+P/wheel click : selects a color from the image'#0+
'ctrl+H/V : flip horizontally/vertically'#0+
'ctrl+R : rotate 90 degrees right (shift -left)'#0+
'F7 : edit current color (or LMB double-click)'#0#0+
'In "Shift Tool", use the cursors to move the image'#0#$ff;

helpConfig: pchar = 
'The application configuration is automatically'#0+
'saved to the ".spoon.cnf" file in the current'#0+
'program directory.'#0#0+
'ctrl+R : restore default configuration'#0#$ff;

procedure WriteHelpLine;
begin
    NeoDrawString(6,helpline,tmpstr);
    inc(helpline,10);
end;

procedure WriteHelp(p:pchar);
var i:word;
    len:byte absolute tmpstr;
begin
    i:=0;
    len:=0;
    while p[i]<>#$ff do begin
        if p[i]=#0 then begin   
            WriteHelpLine;
            len:=0;
        end else begin
            inc(len);
            tmpstr[len]:=p[i];
        end;
        inc(i);
    end;
end;

begin
    NeoSetColor(GUI_BACKDARK);
    NeoDrawLine(4,26,316,26);
    NeoSetColor(GUI_TEXTBOLD);
    NeoSetSolidFlag(0);
    tmpstr:='Help Page :';
    NeoDrawString(6,16,tmpstr);
    tmpstr:=tabnames[page];
    NeoDrawString(78,16,tmpstr);
    NeoSetColor(GUI_TEXT);
    helpline:=30;
    case page of
        0: WriteHelp(helpFile);
        1..3: WriteHelp(helpList);
        4: WriteHelp(helpEditor);
        5: WriteHelp(helpConfig);
    end;
    tmpstr:='Press ESC to return to the program.';
    BottomBar(GUI_TEXT,GUI_BOTTOM,tmpstr);
end;

